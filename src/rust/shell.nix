let
  topLevel = import ../..;
  framework = topLevel.framework;

  inherit (framework.pkgs) dev musl none;

  plat = "virt";

  configured = framework.pkgs.none.icecap.configured.${plat};
  # configured = topLevel.hypervisor.tests.benchmark-server.virt.configured;

  inherit (configured) seL4ForUserspace;
  # libsel4 = toString (none.icecap.icecapFrameworkConfig.source.localPathOf "seL4-bundle" + "/tmp/out");

  capDLLoader = configured.dummyCapDLLoader.passthru;
  # capDLLoader = topLevel.hypervisor.tests.hypervisor.virt.composition.attrs.app.passthru;

  loaderConfig = {
    x = true;
    y = "123";
  };
in

dev.mkShell {
  SEL4_PREFIX = seL4ForUserspace;
  SEL4_LOADER_CONFIG = dev.writeText "loader-config.json" (builtins.toJSON loaderConfig);
  SEL4_APP = "${seL4ForUserspace}/bin/kernel.elf"; # HACK

  CAPDL_SPEC_FILE = capDLLoader.json;
  CAPDL_FILL_DIR = capDLLoader.fill;

  LIBCLANG_PATH = dev.icecap.libclangPath;

  "CC_aarch64-sel4" = "${none.stdenv.cc.targetPrefix}cc";
  "CC_aarch64-unknown-none" = "${none.stdenv.cc.targetPrefix}cc";

  nativeBuildInputs = with dev; [
    rustup
    git
    cacert
    none.stdenv.cc
    none.stdenv.cc.bintools
    musl.stdenv.cc
    musl.stdenv.cc.bintools
  ];
}
