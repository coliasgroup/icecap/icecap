{ mk, versions }:

mk {
  nix.meta.requirements = [ "linux" ];
  package.name = "generated-module-hack";
  lib.proc-macro = true;
  dependencies = {
    inherit (versions) quote;
    syn = { version = versions.syn; features = [ "full" ]; };
  };
}
