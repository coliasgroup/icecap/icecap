{ mk }:

mk {
  package.name = "biterate";
  dependencies = {
    num = { version = "0.4.0"; default-features = false; };
  };
}
