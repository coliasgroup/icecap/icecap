{ mk, versions }:

mk {
  nix.meta.requirements = [ "linux" ];
  package.name = "finite-set-derive";
  lib.proc-macro = true;
  dependencies = {
    inherit (versions) proc-macro2;
    inherit (versions) quote;
    inherit (versions) syn synstructure;
  };
}
