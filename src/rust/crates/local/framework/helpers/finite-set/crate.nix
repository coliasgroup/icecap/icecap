{ mk, localCrates }:

mk {
  package.name = "finite-set";
  nix.local.dependencies = with localCrates; [
    finite-set-derive
  ];
}
