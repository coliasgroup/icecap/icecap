{ mk, versions }:

mk {
  package.name = "icecap-fdt";
  dependencies = {
    inherit (versions) log ;
  };
}
