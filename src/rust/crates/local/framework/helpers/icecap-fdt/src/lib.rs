#![no_std]
#![feature(int_roundings)]

#[macro_use]
extern crate alloc;

mod debug;
mod error;
mod read;
mod types;
mod write;

pub mod bindings;

pub use error::{Error, Result};

pub use types::{DeviceTree, Node, ReserveEntry, Value};
