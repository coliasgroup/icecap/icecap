{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-vmm-utils";
  nix.local.dependencies = with localCrates; [
    sel4
    sel4-supervising
    icecap-failure
    icecap-vmm-gic
  ];
}
