{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-vmm-psci";
  nix.local.dependencies = with localCrates; [
    sel4
  ];
}
