{ mk, localCrates, serdeWith, versions }:

mk {
  package.name = "icecap-vmm-fdt-bindings";
  nix.local.dependencies = with localCrates; [
    icecap-fdt
  ];
  dependencies = {
    inherit (versions) log ;
    serde = serdeWith [ "alloc" "derive" ];
  };
}
