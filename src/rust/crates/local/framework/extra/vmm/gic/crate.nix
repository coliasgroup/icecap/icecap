{ mk, localCrates, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-vmm-gic";
  nix.local.dependencies = with localCrates; [
    biterate
    sel4
    sel4-supervising
    icecap-failure
    icecap-failure-derive
  ];
  dependencies = {
    inherit (versions) log ;
  };
}
