{ mk, localCrates }:

mk {
  package.name = "icecap-std-external";
  nix.local.dependencies = with localCrates; [
    icecap-core
  ];
  dependencies = {
    icecap-core.features = [ "full" ];
  };
  nix.meta.skip = true;
}
