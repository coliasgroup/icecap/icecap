#![feature(panic_info_message)]
#![feature(rustc_private)]

use icecap_core::backtrace;
use icecap_core::runtime::{abort_this_thread, debug_println};

pub use std::icecap_impl::set_now;

pub fn early_init() {
    set_panic()
}

pub fn set_panic() {
    std::panic::set_hook(Box::new(|info| {
        debug_println!("{}", info);
        backtrace::collect_and_send_and_notify_infallible();
        abort_this_thread()
    }));
}
