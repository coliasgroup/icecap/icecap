pub unsafe fn abort() -> ! {
    crate::runtime_interface::abort_this_thread()
}
