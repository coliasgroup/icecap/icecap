{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-std-impl";
  nix.local.dependencies = with localCrates; [
    sel4
    sel4-sync
    sel4-dlmalloc
  ];
}
