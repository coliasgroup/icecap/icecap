#![no_std]

#[cfg(target_env = "sel4")]
extern crate alloc;

use serde::{Deserialize, Serialize};

use sel4_simple_task_config_types::{ConfigCPtr, Notification};

#[cfg(target_env = "sel4")]
mod icecap;
#[cfg(target_env = "sel4")]
pub use icecap::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RingBufferConfig {
    pub read: RingBufferSideConfig,
    pub write: RingBufferSideConfig,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RingBufferSideConfig {
    pub size: usize,
    pub ctrl: usize,
    pub data: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RingBufferKicksConfig<T> {
    pub read: T,
    pub write: T,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UnmanagedRingBufferConfig {
    pub ring_buffer: RingBufferConfig,
    pub kicks: RingBufferKicksConfig<ConfigCPtr<Notification>>,
}
