use alloc::boxed::Box;

use icecap_ring_buffer::{Kick, RingBuffer, RingBufferPointer, RingBufferSide};

use crate::*;

fn mk_kick(nfn: Notification) -> Kick {
    Box::new(move || nfn.signal())
}

fn mk_kicks(
    kicks: &RingBufferKicksConfig<ConfigCPtr<Notification>>,
) -> RingBufferKicksConfig<Kick> {
    RingBufferKicksConfig {
        read: mk_kick(kicks.read.get()),
        write: mk_kick(kicks.write.get()),
    }
}

impl RingBufferConfig {
    pub fn realize(&self, kicks: RingBufferKicksConfig<Kick>) -> RingBuffer {
        RingBuffer::new(
            self.read.realize(kicks.read),
            self.write.realize(kicks.write),
        )
    }

    pub fn realize_resuming(&self, kicks: RingBufferKicksConfig<Kick>) -> RingBuffer {
        RingBuffer::resume(
            self.read.realize(kicks.read),
            self.write.realize(kicks.write),
        )
    }
}

impl UnmanagedRingBufferConfig {
    pub fn realize(&self) -> RingBuffer {
        self.ring_buffer.realize(mk_kicks(&self.kicks))
    }

    pub fn realize_resuming(&self) -> RingBuffer {
        self.ring_buffer.realize_resuming(mk_kicks(&self.kicks))
    }
}

impl RingBufferSideConfig {
    pub fn realize<T: RingBufferPointer>(&self, kick: Kick) -> RingBufferSide<T> {
        RingBufferSide::new(self.size, self.ctrl, T::from_address(self.data), kick)
    }
}
