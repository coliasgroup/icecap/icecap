{ mk, localCrates, serdeWith }:

mk {
  package.name = "icecap-ring-buffer-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
  ];
  nix.local.target."cfg(target_env = \"sel4\")".dependencies = with localCrates; [
    icecap-ring-buffer
  ];
  dependencies = {
    serde = serdeWith [ "derive" ];
  };
}
