#![no_std]
#![feature(stdsimd)]
#![feature(core_intrinsics)]

#[macro_use]
extern crate alloc;

mod buffered_ring_buffer;
mod ring_buffer;

pub use buffered_ring_buffer::{BufferedPacketRingBuffer, BufferedRingBuffer};
pub use ring_buffer::{Kick, PacketRingBuffer, RingBuffer, RingBufferPointer, RingBufferSide};
