{ mk, localCrates, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-ring-buffer";
  nix.local.dependencies = with localCrates; [
    sel4
  ];
  dependencies = {
    inherit (versions) log ;
    inherit (versions) tock-registers;
  };
}
