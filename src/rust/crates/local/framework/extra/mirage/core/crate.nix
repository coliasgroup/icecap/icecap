{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-mirage-core";
  nix.local.dependencies = with localCrates; [
    icecap-core
    icecap-linux-syscall-types
    icecap-linux-syscall-musl
  ];
}
