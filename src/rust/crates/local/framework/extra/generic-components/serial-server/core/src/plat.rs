use icecap_core::sel4::sel4_cfg_if;

sel4_cfg_if! {
    if #[cfg(PLAT_QEMU_ARM_VIRT)] {
        use icecap_pl011_driver::Pl011Device;

        pub fn plat_init_device(base_addr: usize) -> Pl011Device {
            let dev = Pl011Device::new(base_addr);
            dev.init();
            dev
        }
    } else if #[cfg(PLAT_BCM2711)] {
        use icecap_bcm2835_aux_uart_driver::Bcm2835AuxUartDevice;

        pub fn plat_init_device(base_addr: usize) -> Bcm2835AuxUartDevice {
            let dev = Bcm2835AuxUartDevice::new(base_addr);
            dev.init();
            dev
        }
    }
}
