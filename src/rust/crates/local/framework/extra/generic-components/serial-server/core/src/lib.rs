#![no_std]
#![no_main]

extern crate alloc;

mod color;
mod event;
mod fmt;
mod plat;
mod server;

pub type ClientId = usize;

pub use event::Event;
pub use plat::plat_init_device;
pub use server::run;
