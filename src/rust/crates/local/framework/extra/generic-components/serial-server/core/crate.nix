{ mk, localCrates, serdeWith, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-generic-serial-server-core";
  nix.local.dependencies = with localCrates; [
    icecap-core
    icecap-generic-timer-server-client
    icecap-driver-interfaces
    icecap-ring-buffer

    # TODO see note in timer-server/crate.nix
    icecap-pl011-driver
    icecap-bcm2835-aux-uart-driver
  ];
  dependencies = {
    icecap-core.features = [ "full" ];
    inherit (versions) cfg-if ;
    serde = serdeWith [ "alloc" "derive" ];
  };
}
