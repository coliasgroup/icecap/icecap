{ mk, localCrates, postcardCommon, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "icecap-serialize-generic-component-config";
  nix.local.dependencies = with localCrates; [
    icecap-component-config-cli-core
    icecap-generic-timer-server-config
  ];

  dependencies = {
    inherit (versions) serde ;
    inherit (versions) serde_json ;
    postcard = postcardCommon;
  };
}
