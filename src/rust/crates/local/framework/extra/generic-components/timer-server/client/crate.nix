{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-generic-timer-server-client";
  nix.local.dependencies = with localCrates; [
    sel4
    sel4-simple-task-rpc
    icecap-generic-timer-server-types
  ];
  dependencies = {
    sel4-simple-task-rpc.features = [ "postcard" ];
  };
}
