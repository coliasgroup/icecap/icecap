{ mk, localCrates, serdeWith }:

mk {
  package.name = "icecap-generic-timer-server-types";
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
