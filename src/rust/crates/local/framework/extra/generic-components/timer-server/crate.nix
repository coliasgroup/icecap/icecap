{ mk, localCrates, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-generic-timer-server";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    icecap-generic-timer-server-types
    icecap-generic-timer-server-config

    icecap-driver-interfaces

    # TODO
    # We should only incur dependencies relevant to the platform. However,
    # target.'cfg(...)'.dependencies is limited. It may the case that, for now,
    # features corresponding to platforms is the only way to do this.
    icecap-virt-timer-driver
    icecap-bcm-system-timer-driver
  ];
  dependencies = {
    inherit (versions) cfg-if ;
    inherit (versions) tock-registers;
  };
}
