use icecap_core::sel4::sel4_cfg_if;

sel4_cfg_if! {
    if #[cfg(PLAT_QEMU_ARM_VIRT)] {
        use icecap_virt_timer_driver::VirtTimerDevice;

        pub fn timer_device(base_addr: usize) -> VirtTimerDevice {
            VirtTimerDevice::new(base_addr)
        }
    } else if #[cfg(PLAT_BCM2711)] {
        use icecap_bcm_system_timer_driver::BcmSystemTimerDevice;

        pub fn timer_device(base_addr: usize) -> BcmSystemTimerDevice {
            BcmSystemTimerDevice::new(base_addr)
        }
    }
}
