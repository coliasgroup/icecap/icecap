#![no_std]

extern crate alloc;

use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub lock: ConfigCPtr<Notification>,
    pub dev_vaddr: usize,
    pub irq_handlers: Vec<ConfigCPtr<IRQHandler>>,
    pub clients: Vec<ConfigCPtr<Notification>>,
    pub endpoints: Vec<ConfigCPtr<Endpoint>>,
    pub secondary_threads: Vec<ConfigCPtr<StaticThread>>,
}
