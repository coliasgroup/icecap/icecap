#![no_std]

use icecap_generic_timer_server_types::{
    Nanoseconds, Request, Result as TimerServerResult, TimerID,
};
use sel4::Endpoint;
use sel4_simple_task_rpc as rpc;

#[derive(Clone)]
pub struct TimerClient {
    ep: rpc::easy::Client<Request>,
}

impl TimerClient {
    pub fn new(ep: Endpoint) -> Self {
        Self {
            ep: rpc::easy::Client::new(ep),
        }
    }

    pub fn completed(&self) -> Result<TimerServerResult<()>, rpc::easy::Error> {
        self.ep.call(&Request::Completed)
    }

    pub fn periodic(
        &self,
        tid: TimerID,
        ns: Nanoseconds,
    ) -> Result<TimerServerResult<()>, rpc::easy::Error> {
        self.ep.call(&Request::Periodic { tid, ns })
    }

    pub fn oneshot_absolute(
        &self,
        tid: TimerID,
        ns: Nanoseconds,
    ) -> Result<TimerServerResult<()>, rpc::easy::Error> {
        self.ep.call(&Request::OneshotAbsolute { tid, ns })
    }

    pub fn oneshot_relative(
        &self,
        tid: TimerID,
        ns: Nanoseconds,
    ) -> Result<TimerServerResult<()>, rpc::easy::Error> {
        self.ep.call(&Request::OneshotRelative { tid, ns })
    }

    pub fn stop(&self, tid: TimerID) -> Result<TimerServerResult<()>, rpc::easy::Error> {
        self.ep.call(&Request::Stop { tid })
    }

    pub fn time(&self) -> Result<Nanoseconds, rpc::easy::Error> {
        self.ep.call(&Request::Time)
    }
}
