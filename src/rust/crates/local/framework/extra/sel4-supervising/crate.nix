{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "sel4-supervising";
  nix.local.dependencies = with localCrates; [
    sel4
  ];
}
