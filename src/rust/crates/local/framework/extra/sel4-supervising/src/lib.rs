#![no_std]

use sel4::{
    sys, IPCBuffer, MessageInfoBuilder, UnknownSyscall, UserContext, VCPUFault, VMFault, Word,
};

macro_rules! inner_decls {
    ($ty:ty) => {
        fn inner(&self) -> &$ty;

        fn inner_mut(&mut self) -> &mut $ty;
    };
}

macro_rules! self_impl {
    ($trt:ty, $ty:ty) => {
        impl $trt for $ty {
            fn inner(&self) -> &Self {
                self
            }

            fn inner_mut(&mut self) -> &mut Self {
                self
            }
        }
    };
}

self_impl!(UserContextExt, UserContext);

pub trait UserContextExt {
    inner_decls!(UserContext);

    fn advance_pc(&mut self) {
        *self.inner_mut().pc_mut() += 4;
    }

    fn read_gpr(&self, ix: Word) -> Word {
        match ix {
            31 => 0,
            _ => *self.inner().gpr(ix),
        }
    }
}

// === faults ===

self_impl!(UnknownSyscallExt, UnknownSyscall);

pub trait UnknownSyscallExt {
    inner_decls!(UnknownSyscall);

    fn reply(num_regs: usize) {
        sel4::with_borrow_ipc_buffer_mut(|ipc_buffer| {
            sel4::reply(
                ipc_buffer,
                MessageInfoBuilder::default().length(num_regs).build(),
            )
        })
    }

    fn advance(&self) {
        sel4::with_borrow_ipc_buffer_mut(|ipc_buffer| {
            *Self::mr_pc(ipc_buffer) = self.inner().fault_ip() + 4;
        })
    }

    fn advance_and_reply(&self) {
        self.inner().advance();
        Self::reply(Self::num_regs_for_gprs_and_pc())
    }

    fn num_regs_for_gprs_and_pc() -> usize {
        sys::seL4_UnknownSyscall_Msg::seL4_UnknownSyscall_FaultIP as usize + 1
    }

    fn mr_pc(ipcbuf: &mut IPCBuffer) -> &mut Word {
        let reg_idx = sys::seL4_UnknownSyscall_Msg::seL4_UnknownSyscall_FaultIP as usize;
        &mut ipcbuf.msg_regs_mut()[reg_idx]
    }

    fn mr_gpr(ipcbuf: &mut IPCBuffer, ix: usize) -> &mut Word {
        assert!(ix <= 7);
        let reg_idx = sys::seL4_UnknownSyscall_Msg::seL4_UnknownSyscall_X0 as usize + ix;
        &mut ipcbuf.msg_regs_mut()[reg_idx]
    }
}

self_impl!(VCPUFaultExt, VCPUFault);

pub trait VCPUFaultExt {
    inner_decls!(VCPUFault);

    fn is_wf(&self) -> bool {
        // AArch64 ESR_EL{1,2}
        self.inner().hsr() >> 26 == 1
    }
}

// TODO remove magic values

const HSR_SYNDROME_VALID: Word = 1 << 24;
const SRT_MASK: Word = 0x1f;

self_impl!(VMFaultExt, VMFault);

pub trait VMFaultExt {
    inner_decls!(VMFault);

    fn is_valid(&self) -> bool {
        self.inner().fsr() & HSR_SYNDROME_VALID != 0
    }

    fn valid_hsr(&self) -> u64 {
        assert!(self.is_valid());
        self.inner().fsr()
    }

    fn is_aligned(&self) -> bool {
        let mask = match self.inner().width() {
            VMFaultWidth::Byte => 0x0,
            VMFaultWidth::HalfWord => 0x1,
            VMFaultWidth::Word => 0x3,
            VMFaultWidth::DoubleWord => 0x7,
        };
        self.inner().addr() & mask == 0
    }

    fn is_write(&self) -> bool {
        self.valid_hsr() & (1 << 6) != 0
    }

    fn is_read(&self) -> bool {
        !self.is_write()
    }

    fn width(&self) -> VMFaultWidth {
        match (self.valid_hsr() >> 22) & 0x3 {
            0 => VMFaultWidth::Byte,
            1 => VMFaultWidth::HalfWord,
            2 => VMFaultWidth::Word,
            3 => VMFaultWidth::DoubleWord,
            _ => unreachable!(),
        }
    }

    fn gpr_index(&self) -> u64 {
        (self.valid_hsr() >> 16) & SRT_MASK
    }

    fn data(&self, ctx: &UserContext) -> VMFaultData {
        assert!(self.is_write());
        self.width().truncate(ctx.read_gpr(self.gpr_index()))
    }

    fn emulate_read(&self, ctx: &mut UserContext, val: VMFaultData) {
        assert!(self.is_read());
        assert_eq!(self.inner().width(), val.into());
        let gpr = ctx.gpr_mut(self.gpr_index());
        *gpr = val.set(*gpr);
    }
}

//

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub enum VMFaultWidth {
    Byte,
    HalfWord,
    Word,
    DoubleWord,
}

impl VMFaultWidth {
    pub fn mask(self) -> u64 {
        match self {
            Self::Byte => 0xff,
            Self::HalfWord => 0xffff,
            Self::Word => 0xffff_ffff,
            Self::DoubleWord => !0,
        }
    }

    pub fn truncate(self, val: u64) -> VMFaultData {
        let val = val & self.mask();
        match self {
            Self::Byte => VMFaultData::Byte(val as u8),
            Self::HalfWord => VMFaultData::HalfWord(val as u16),
            Self::Word => VMFaultData::Word(val as u32),
            Self::DoubleWord => VMFaultData::DoubleWord(val as u64),
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum VMFaultData {
    Byte(u8),
    HalfWord(u16),
    Word(u32),
    DoubleWord(u64),
}

impl VMFaultData {
    pub fn set(self, old_val: u64) -> u64 {
        (old_val & !VMFaultWidth::from(self).mask()) | u64::from(self)
    }
}

impl From<VMFaultData> for u64 {
    fn from(x: VMFaultData) -> u64 {
        match x {
            VMFaultData::Byte(raw) => raw as u64,
            VMFaultData::HalfWord(raw) => raw as u64,
            VMFaultData::Word(raw) => raw as u64,
            VMFaultData::DoubleWord(raw) => raw as u64,
        }
    }
}

impl From<VMFaultData> for VMFaultWidth {
    fn from(x: VMFaultData) -> VMFaultWidth {
        match x {
            VMFaultData::Byte(_) => Self::Byte,
            VMFaultData::HalfWord(_) => Self::HalfWord,
            VMFaultData::Word(_) => Self::Word,
            VMFaultData::DoubleWord(_) => Self::DoubleWord,
        }
    }
}
