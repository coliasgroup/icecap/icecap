#![no_std]
#![feature(auto_traits)]
#![feature(negative_impls)]

#[macro_use]
extern crate alloc;

use alloc::collections::btree_map::BTreeMap;
use alloc::string::String;
use alloc::vec::Vec;

use icecap_capdl_dynamic_types::*;
use icecap_core::prelude::*;

mod allocator;
mod cregion;
mod initialize_subsystem_objects;
mod spec_view;
mod utils;

use spec_view::SpecView;
use utils::size_bits_to_contain;

pub use allocator::{Allocator, AllocatorBuilder};
pub use cregion::{CRegion, Slot};
pub use initialize_subsystem_objects::SubsystemObjectInitializationResources;

/// Unique id for an untyped block of memory
#[derive(Clone, Debug)]
pub struct UntypedId {
    /// The physical address of the untyped block
    pub paddr: usize,

    /// log_2 of the size (in bytes) of the untyped block
    /// (e.g., for a 32 byte block, `size_bits` is 5)
    pub size_bits: usize,
}

pub struct ElaboratedUntyped {
    pub cptr: Untyped,
    pub untyped_id: UntypedId,
}

pub type Externs = BTreeMap<String, Extern>;

pub struct Extern {
    pub ty: ExternObject,
    pub cptr: Unspecified,
}

pub struct Realizer {
    pub initialization_resources: SubsystemObjectInitializationResources,
    pub allocator: Allocator,
    pub externs: Externs,
}

pub struct Subsystem {
    pub virtual_cores: Vec<VirtualCore>,

    // ID of the Untyped retyped for the realm's CNode.
    pub cnode_untyped_id: UntypedId,

    // IDs of the Untypeds retyped for the realm's objects.
    pub object_untyped_ids: Vec<UntypedId>,

    pub externs: Externs,
}

pub type VirtualCores = Vec<VirtualCore>;

pub struct VirtualCore {
    pub tcbs: Vec<VirtualCoreTCB>,
}

pub struct VirtualCoreTCB {
    pub cap: TCB,
    pub resume: bool,
}

pub struct PartialSubsystem {
    pub spec: DynamicSpec<DynamicFill>,
    pub spec_view: SpecView,
    pub cregion: CRegion,
    pub cnode_untyped_id: UntypedId,
    pub local_object_slots: Vec<Slot>,
    pub local_object_untyped_ids: Vec<UntypedId>,
}

impl Realizer {
    pub fn new(
        initialization_resources: SubsystemObjectInitializationResources,
        allocator: Allocator,
        externs: Externs,
    ) -> Self {
        Realizer {
            initialization_resources,
            allocator,
            externs,
        }
    }

    pub fn destroy(&mut self, subsystem: Subsystem) -> Fallible<()> {
        self.allocator
            .revoke_and_free(&subsystem.cnode_untyped_id)?;
        for untyped_id in &subsystem.object_untyped_ids {
            self.allocator.revoke_and_free(&untyped_id)?;
        }
        self.externs.extend(subsystem.externs);
        Ok(())
    }

    pub fn realize_start(&mut self, spec: DynamicSpec<DynamicFill>) -> Fallible<PartialSubsystem> {
        let view = SpecView::new(&spec);

        let cnode_slots_size_bits = {
            let mut num_frame_mappings: usize = 0;
            for i in view.local_objects.iter() {
                if let DynamicObject::Local(obj) = &spec.objects[*i].object {
                    match obj {
                        Object::PageTable(obj) => match obj.level.unwrap() {
                            3 => {
                                num_frame_mappings += obj.slots().len();
                            }
                            2 => {
                                for (_, entry) in obj.entries() {
                                    if let PageTableEntry::Frame(_) = entry {
                                        num_frame_mappings += 1;
                                    }
                                }
                            }
                            _ => {}
                        },
                        _ => {}
                    }
                }
            }
            size_bits_to_contain(view.local_objects.len() + num_frame_mappings)
        };

        let local_object_blueprints: Vec<ObjectBlueprint> = view
            .local_objects
            .iter()
            .map(|i| match &spec.objects[*i].object {
                DynamicObject::Local(obj) => obj.blueprint().unwrap(), // HACK
                _ => panic!(),
            })
            .collect();

        let untyped_requirements: Vec<usize> = {
            let mut v = vec![0; 64];
            v[ObjectBlueprint::CNode {
                size_bits: cnode_slots_size_bits,
            }
            .physical_size_bits()] += 1;
            for blueprint in &local_object_blueprints {
                v[blueprint.physical_size_bits()] += 1
            }
            v
        };
        ensure!(self.allocator.peek_space(&untyped_requirements));

        let (mut cregion, cnode_untyped_id, _managed_cnode_slot) =
            self.allocator.create_cnode(cnode_slots_size_bits)?;
        let (local_object_slots, local_object_untyped_ids) = self
            .allocator
            .create_objects(&mut cregion, &local_object_blueprints)?;

        let spec_view = SpecView::new(&spec);

        Ok(PartialSubsystem {
            spec,
            spec_view,
            cregion,
            cnode_untyped_id,
            local_object_slots,
            local_object_untyped_ids,
        })
    }

    pub fn realize_continue(
        &self,
        partial: &PartialSubsystem,
        obj_id: ObjectId,
        fill_entry_index: usize,
        untrusted_content: &[u8],
    ) -> Fallible<()> {
        let obj = &partial.spec.objects[obj_id];
        let slot = partial.local_object_slots[partial.spec_view.reverse[obj_id]];
        if let DynamicObject::Local(obj) = &obj.object {
            match obj {
                Object::Frame(frame) => match frame.size_bits {
                    12 => {
                        self.initialization_resources.fill_frame(
                            partial.cregion.local_cptr::<cap_type::SmallPage>(slot),
                            frame.init.as_fill().unwrap().entries[fill_entry_index]
                                .range
                                .start,
                            match &frame.init.as_fill().unwrap().entries[fill_entry_index].content {
                                FillEntryContent::Data(data) => &data.content_digest,
                                _ => panic!(),
                            },
                            untrusted_content,
                        )?;
                    }
                    21 => {
                        self.initialization_resources.fill_frame(
                            partial.cregion.local_cptr::<cap_type::LargePage>(slot),
                            frame.init.as_fill().unwrap().entries[fill_entry_index]
                                .range
                                .start,
                            match &frame.init.as_fill().unwrap().entries[fill_entry_index].content {
                                FillEntryContent::Data(data) => &data.content_digest,
                                _ => panic!(),
                            },
                            untrusted_content,
                        )?;
                    }
                    _ => {
                        panic!()
                    }
                },
                _ => {
                    panic!()
                }
            }
        } else {
            panic!()
        }
        Ok(())
    }

    pub fn realize_finish(&mut self, mut partial: PartialSubsystem) -> Fallible<Subsystem> {
        let (externs, extern_caps): (Externs, Vec<Unspecified>) = {
            let mut externs = BTreeMap::new();
            let extern_caps: Vec<Unspecified> = partial
                .spec_view
                .extern_objects
                .iter()
                .map(|i| match &partial.spec.objects[*i].object {
                    DynamicObject::Extern(obj) => {
                        let name = partial.spec.objects[*i].name.clone();
                        let ext = self.externs.remove(&name).unwrap();
                        assert_eq!(&ext.ty, obj);
                        let cptr = ext.cptr;
                        externs.insert(name, ext);
                        cptr
                    }
                    _ => panic!(),
                })
                .collect();
            (externs, extern_caps)
        };

        let all_caps: Vec<Unspecified> = partial
            .spec
            .objects
            .iter()
            .enumerate()
            .map(|(i, obj)| match &obj.object {
                DynamicObject::Local(_) => partial.cregion.local_cptr::<cap_type::Unspecified>(
                    partial.local_object_slots[partial.spec_view.reverse[i]],
                ),
                DynamicObject::Extern(_) => extern_caps[partial.spec_view.reverse[i]],
            })
            .collect();

        let virtual_cores = self.initialization_resources.initialize(
            partial.spec.num_nodes,
            &partial.spec,
            &all_caps,
            &mut partial.cregion,
        )?;

        Ok(Subsystem {
            virtual_cores,
            cnode_untyped_id: partial.cnode_untyped_id,
            object_untyped_ids: partial.local_object_untyped_ids,
            externs,
        })
    }
}
