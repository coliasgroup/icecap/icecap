#![allow(unused_variables)]

use core::ops::Range;

use icecap_core::prelude::*;

mod set;

use set::Set;

/// CRegion is a CNode with associated metadata.
#[derive(Debug)]
pub struct CRegion {
    /// AbsoluteCPtr to the CNode represented by this CRegion.
    pub root: AbsoluteCPtr,

    /// Guard of the CRegion's CNode.
    pub guard: u64,

    /// Bits in the guard of the CRegion's CNode.
    pub guard_size: u64,

    /// log_2 of the size of the CRegion's CNode.
    pub radix: usize,

    set: Set,
}

pub type Slot = usize;

impl CRegion {
    pub fn new(root: AbsoluteCPtr, guard: u64, guard_size: u64, radix: usize) -> Self {
        let mut set = Set::new();
        set.deposit(0..1 << radix);
        Self {
            root,
            guard,
            guard_size,
            radix,
            set,
        }
    }

    /// Allocate a single slot in the cregion.
    pub fn alloc(&mut self) -> Option<Slot> {
        match self.set.withdraw(1) {
            Some(range) => Some(range.start),
            None => None,
        }
    }

    pub fn alloc_range(&mut self, size: usize) -> Range<Slot> {
        todo!()
    }

    /// Free a single slot in the cregion.
    pub fn free(&mut self, slot: Slot) {
        self.set.deposit(slot..(slot + 1));
    }

    pub fn free_range(&mut self, slots: Range<Slot>) {
        todo!()
    }

    /// Create a CPtrWithDepth to a capability in a specified Slot.
    pub fn cptr_with_depth(&self, slot: Slot) -> CPtrWithDepth {
        let slot = u64::try_from(slot).unwrap();
        let radix = u64::try_from(self.radix).unwrap();

        let mut cptr_bits: u64 = self.root.path().bits();

        cptr_bits = (cptr_bits << self.guard_size) | self.guard;

        cptr_bits = (cptr_bits << radix) | slot;

        let depth =
            self.root.path().depth() + usize::try_from(self.guard_size).unwrap() + self.radix;

        CPtrWithDepth::from_bits_with_depth(cptr_bits, depth)
    }

    /// Create a AbsoluteCPtr to a capability in a specified Slot.
    pub fn relative_cptr(&self, slot: Slot) -> AbsoluteCPtr {
        self.context().relative(self.cptr_with_depth(slot))
    }

    /// Create a AbsoluteCPtr to a capability in a specified Slot.
    pub fn local_cptr<T: CapType>(&self, slot: Slot) -> LocalCPtr<T> {
        let with_depth = self.cptr_with_depth(slot);
        assert_eq!(with_depth.depth(), WORD_SIZE);
        LocalCPtr::from_bits(with_depth.bits())
    }

    /// Create a AbsoluteCPtr to a capability in a specified Slot.
    pub fn local_cptr_assuming_cspace_hack<T: CapType>(&self, slot: Slot) -> LocalCPtr<T> {
        let with_depth = self.cptr_with_depth(slot);
        LocalCPtr::from_bits(with_depth.bits() << (WORD_SIZE - with_depth.depth()))
    }

    pub fn context(&self) -> &CNode {
        self.root.root()
    }
}
