{ mk, localCrates, serdeWith, postcardCommon, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-capdl-dynamic-realize";
  nix.local.dependencies = with localCrates; [
    icecap-capdl-dynamic-types
    icecap-core
  ];
  dependencies = {
    icecap-capdl-dynamic-types.features = [ "sel4" ];
    icecap-core.features = [ "alloc" ];
    inherit (versions) log ;
    sha2 = { version = "0.10.6"; default-features = false; };
  };
}
