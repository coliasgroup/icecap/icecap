use alloc::vec::Vec;

use icecap_capdl_dynamic_types::*;

pub struct SpecView {
    pub local_objects: Vec<usize>, // sorted by size_bits and then by type
    pub extern_objects: Vec<usize>,
    pub reverse: Vec<usize>,
}

impl SpecView {
    pub fn new(spec: &DynamicSpec<DynamicFill>) -> Self {
        let mut view = Self {
            local_objects: vec![],
            extern_objects: vec![],
            reverse: vec![0; spec.objects.len()],
        };

        for (i, obj) in spec.objects.iter().enumerate() {
            match &obj.object {
                DynamicObject::Local(_) => {
                    view.local_objects.push(i);
                }
                DynamicObject::Extern(_) => {
                    view.reverse[i] = view.extern_objects.len();
                    view.extern_objects.push(i);
                }
            }

            view.local_objects.sort_by_key(|i| {
                let blueprint = match &spec.objects[*i].object {
                    DynamicObject::Local(obj) => obj.blueprint().unwrap(), // HACK
                    _ => panic!(),
                };
                (blueprint.physical_size_bits(), blueprint)
            });

            for (i, j) in view.local_objects.iter().enumerate() {
                view.reverse[*j] = i;
            }
        }

        view
    }
}
