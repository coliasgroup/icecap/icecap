pub fn size_bits_to_contain(x: usize) -> usize {
    const TOTAL: usize = 8 * core::mem::size_of::<usize>();
    TOTAL - (x.leading_zeros() as usize)
}
