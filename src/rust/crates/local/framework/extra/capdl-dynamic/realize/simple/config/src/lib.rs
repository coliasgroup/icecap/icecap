#![no_std]

extern crate alloc;

use alloc::collections::btree_map::BTreeMap;
use alloc::string::String;
use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use icecap_capdl_dynamic_types::ExternObject;
use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RealizerConfig {
    pub initialization_resources: SubsystemObjectInitializationResourcesConfig,
    pub small_page: ConfigCPtr<SmallPage>,
    pub large_page: ConfigCPtr<LargePage>,

    pub allocator_cregion: CRegionConfig,
    pub untyped: Vec<DynamicUntyped>,
    pub externs: ExternsConfig,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SubsystemObjectInitializationResourcesConfig {
    pub pgd: ConfigCPtr<PGD>,
    pub asid_pool: ConfigCPtr<ASIDPool>,
    pub tcb_authority: ConfigCPtr<TCB>,
    pub small_page_addr: usize,
    pub large_page_addr: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CRegionConfig {
    pub root: AbsoluteCPtrConfig,
    pub guard: u64,
    pub guard_size: u64,
    pub slots_size_bits: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AbsoluteCPtrConfig {
    pub root: ConfigCPtr<CNode>,
    pub cptr: ConfigCPtr<CPtr>,
    pub depth: usize,
}

pub type ExternsConfig = BTreeMap<String, ExternsConfigEntry>;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct ExternsConfigEntry {
    pub ty: ExternObject,
    pub cptr: u64,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DynamicUntyped {
    pub slot: ConfigCPtr<Untyped>,
    pub size_bits: usize,
    pub paddr: Option<usize>,
    pub device: bool,
}
