use core::slice;

use sha2::{Digest, Sha256};

use icecap_capdl_dynamic_types::*;
use icecap_core::prelude::*;
use icecap_core::sel4::{CNodeCapData, CapRights, UserContext, VMAttributes};

use crate::{CRegion, VirtualCore, VirtualCoreTCB, VirtualCores};

pub struct SubsystemObjectInitializationResources {
    pub pgd: PGD,
    pub asid_pool: ASIDPool,
    pub tcb_authority: TCB,
    pub small_page_addr: usize,
    pub large_page_addr: usize,
}

impl SubsystemObjectInitializationResources {
    pub fn fill_frame<T: FrameType>(
        &self,
        frame: LocalCPtr<T>,
        offset: usize,
        expected_content_digest: &[u8],
        untrusted_content: &[u8],
    ) -> Fallible<()> {
        let vaddr = match T::FRAME_SIZE {
            FrameSize::Small => self.small_page_addr,
            FrameSize::Large => self.large_page_addr,
            _ => panic!(),
        };
        frame.frame_map(
            self.pgd,
            vaddr,
            CapRights::read_write(),
            VMAttributes::default() & !VMAttributes::PAGE_CACHEABLE,
        )?;
        let view = unsafe { slice::from_raw_parts_mut(vaddr as *mut u8, T::FRAME_SIZE.bytes()) };
        let refined_view = &mut view[offset..(offset + untrusted_content.len())];
        refined_view.copy_from_slice(&untrusted_content);
        {
            let mut hasher = Sha256::new();
            hasher.update(refined_view);
            let digest = hasher.finalize();
            ensure!(digest.as_slice() == expected_content_digest);
        }
        frame.frame_unmap()?;
        Ok(())
    }

    pub fn initialize(
        &self,
        num_nodes: usize,
        spec: &DynamicSpec<DynamicFill>,
        caps: &[Unspecified],
        cregion: &mut CRegion,
    ) -> Fallible<VirtualCores> {
        Initialize {
            initialization_resources: self,
            num_nodes,
            spec,
            caps: &caps,
            cregion,
        }
        .initialize()
    }
}

struct Initialize<'a> {
    initialization_resources: &'a SubsystemObjectInitializationResources,
    num_nodes: usize,
    spec: &'a DynamicSpec<DynamicFill>,
    caps: &'a [Unspecified],
    cregion: &'a mut CRegion,
}

impl<'a> Initialize<'a> {
    fn initialize(&mut self) -> Fallible<VirtualCores> {
        self.init_vspaces()?;
        self.init_cspaces()?;
        let mut virtual_cores = (0..self.num_nodes)
            .map(|_| VirtualCore { tcbs: vec![] })
            .collect::<VirtualCores>();
        for (i, obj) in Self::it_i::<&object::TCB>(self.spec) {
            let tcb = self.cap(i);
            let name = &self.spec.objects[i].name;
            self.init_tcb(tcb, obj)?;
            tcb.debug_name(name.as_bytes());
            virtual_cores[obj.extra.affinity as usize]
                .tcbs
                .push(VirtualCoreTCB {
                    cap: tcb,
                    resume: obj.extra.resume,
                });
        }
        Ok(virtual_cores)
    }

    fn init_vspaces(&mut self) -> Fallible<()> {
        for (pgd, obj) in Self::it_cap::<cap_type::PGD, &object::PageTable>(self.spec, self.caps) {
            if !obj.is_root {
                continue;
            }
            self.initialization_resources
                .asid_pool
                .asid_pool_assign(pgd)?;

            for (i, cap) in obj.page_tables() {
                let obj: &object::PageTable = self.obj(cap.object)?;
                let pud: PUD = self.cap(cap.object);
                let vaddr = i << (9 + 9 + 9 + 12);
                let attrs = VMAttributes::default();
                pud.pud_map(pgd, vaddr, attrs)?;

                for (i, cap) in obj.page_tables() {
                    let obj: &object::PageTable = self.obj(cap.object)?;
                    let pd: PD = self.cap(cap.object);
                    let vaddr = vaddr + (i << (9 + 9 + 12));
                    let attrs = VMAttributes::default();
                    pd.pd_map(pgd, vaddr, attrs)?;

                    for (i, cap) in obj.entries() {
                        let vaddr = vaddr + (i << (9 + 12));
                        match cap {
                            PageTableEntry::Frame(cap) => {
                                let orig_frame: LargePage = self.cap(cap.object);
                                let frame: LargePage = self.copy(orig_frame)?;
                                let rights = (&cap.rights).into();
                                // TODO more VMAttributes from cdl
                                let attrs = VMAttributes::default()
                                    & !(if !cap.cached {
                                        VMAttributes::PAGE_CACHEABLE
                                    } else {
                                        VMAttributes::NONE
                                    });
                                frame.frame_map(pgd, vaddr, rights, attrs)?;
                            }
                            PageTableEntry::PageTable(cap) => {
                                let obj: &object::PageTable = self.obj(cap.object)?;
                                let pt: PT = self.cap(cap.object);
                                let attrs = VMAttributes::default();
                                pt.pt_map(pgd, vaddr, attrs)?;

                                for (i, cap) in obj.frames() {
                                    let orig_frame: SmallPage = self.cap(cap.object);
                                    let frame: SmallPage = self.copy(orig_frame)?;
                                    let vaddr = vaddr + (i << 12);
                                    let rights = (&cap.rights).into();
                                    // TODO more VMAttributes from cdl
                                    let attrs = VMAttributes::default()
                                        & !(if !cap.cached {
                                            VMAttributes::PAGE_CACHEABLE
                                        } else {
                                            VMAttributes::NONE
                                        });
                                    frame.frame_map(pgd, vaddr, rights, attrs)?;
                                }
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }

    fn init_cspaces(&self) -> Fallible<()> {
        for (cnode, obj) in self.it::<cap_type::CNode, &object::CNode>() {
            for (i, cap) in obj.slots() {
                // TODO enforce per-extern policy
                let dst = cnode.relative_bits_with_depth(*i as u64, obj.size_bits);
                let mut rights = CapRights::all();
                let mut badge = None;
                let ptr = match cap {
                    Cap::Untyped(cap) => cap.object,
                    Cap::Endpoint(cap) => {
                        badge = Some(cap.badge);
                        rights = (&cap.rights).into();
                        cap.object
                    }
                    Cap::Notification(cap) => {
                        badge = Some(cap.badge);
                        rights = (&cap.rights).into();
                        cap.object
                    }
                    Cap::CNode(cap) => {
                        badge = Some(
                            CNodeCapData::new(cap.guard, cap.guard_size.try_into().unwrap())
                                .into_word(),
                        );
                        cap.object
                    }
                    Cap::TCB(cap) => cap.object,
                    Cap::VCPU(cap) => cap.object,
                    Cap::PageTable(cap) => cap.object,
                    Cap::Frame(cap) => {
                        rights = (&cap.rights).into();
                        cap.object
                    }
                    _ => {
                        bail!("unsupported cap {:?}", cap)
                    }
                };
                let src = self
                    .cregion
                    .root
                    .root()
                    .relative(self.cap::<cap_type::Unspecified>(ptr));
                match badge {
                    // HACK 0-badge != no-badge
                    None | Some(0) => dst.copy(&src, rights),
                    Some(badge) => dst.mint(&src, rights, badge),
                }?;
            }
        }
        Ok(())
    }

    fn init_tcb(&self, tcb: TCB, obj: &object::TCB) -> Fallible<()> {
        let fault_ep = sel4::CPtr::from_bits(obj.extra.master_fault_ep.unwrap());
        let cspace = self.cap(obj.cspace().object);
        let cspace_root_data = CNodeCapData::new(
            obj.cspace().guard,
            obj.cspace().guard_size.try_into().unwrap(),
        );
        let vspace = self.cap(obj.vspace().object);
        let ipc_buffer_frame = self.cap(obj.ipc_buffer().object);

        if let Some(vcpu) = obj.vcpu() {
            let vcpu: VCPU = self.cap(vcpu.object);
            vcpu.vcpu_set_tcb(tcb)?;
        }

        if let Some(bound_notification) = obj.bound_notification() {
            let bound_notification: Notification = self.cap(bound_notification.object);
            tcb.tcb_bind_notification(bound_notification)?;
        }

        tcb.tcb_configure(
            fault_ep,
            cspace,
            cspace_root_data,
            vspace,
            obj.extra.ipc_buffer_addr,
            ipc_buffer_frame,
        )?;
        tcb.tcb_set_sched_params(
            self.initialization_resources.tcb_authority,
            obj.extra.max_prio as u64,
            obj.extra.prio as u64,
        )?;

        let mut regs = UserContext::default();
        *regs.pc_mut() = obj.extra.ip;
        *regs.sp_mut() = obj.extra.sp;
        *regs.spsr_mut() = obj.extra.spsr;
        for (i, value) in obj.extra.gprs.iter().enumerate() {
            *regs.gpr_mut(i.try_into()?) = *value;
        }
        tcb.tcb_write_all_registers(false, &mut regs)?;

        Ok(())
    }

    ////

    fn copy<T: CapType + NotCNodeCapType>(&mut self, cap: LocalCPtr<T>) -> Fallible<LocalCPtr<T>> {
        let slot = self.cregion.alloc().unwrap();
        let src = self.cregion.context().relative(cap);
        self.cregion
            .relative_cptr(slot)
            .copy(&src, CapRights::all())?;
        Ok(self.cregion.local_cptr(slot))
    }

    fn obj<O: TryFrom<&'a LocalObject<DynamicFill>>>(&self, i: usize) -> Fallible<O> {
        if let DynamicObject::Local(obj) = &self.spec.objects[i].object {
            Ok(O::try_from(&obj).ok().unwrap()) // TODO
        } else {
            bail!("")
        }
    }

    fn cap<T: CapType>(&self, i: usize) -> LocalCPtr<T> {
        self.caps[i].downcast()
    }

    fn it_i<T: TryFrom<&'a LocalObject<DynamicFill>>>(
        spec: &'a DynamicSpec<DynamicFill>,
    ) -> impl Iterator<Item = (usize, T)> + 'a {
        spec.objects.iter().enumerate().filter_map(|(i, obj)| {
            let obj: T = match &obj.object {
                DynamicObject::Local(obj) => T::try_from(obj).ok(),
                _ => None,
            }?;
            Some((i, obj))
        })
    }

    fn it_cap<O: CapType + 'a, T: TryFrom<&'a LocalObject<DynamicFill>> + 'a>(
        spec: &'a DynamicSpec<DynamicFill>,
        caps: &'a [Unspecified],
    ) -> impl Iterator<Item = (LocalCPtr<O>, T)> + 'a {
        Self::it_i(spec).map(move |(i, obj)| (caps[i].downcast(), obj))
    }

    fn it<O: CapType + 'a, T: TryFrom<&'a LocalObject<DynamicFill>> + 'a>(
        &'a self,
    ) -> impl Iterator<Item = (LocalCPtr<O>, T)> + 'a {
        Self::it_cap(&self.spec, &self.caps)
    }
}

////

// HACK until we get negative reasoning
pub auto trait NotCNodeCapType {}

impl !NotCNodeCapType for sel4::cap_type::CNode {}
