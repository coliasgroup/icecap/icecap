{ mk, localCrates, serdeWith }:

mk {
  package.name = "icecap-capdl-dynamic-realize-simple-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    icecap-capdl-dynamic-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
