{ mk, localCrates, serdeWith, postcardCommon }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-capdl-dynamic-realize-simple";
  nix.local.dependencies = with localCrates; [
    icecap-capdl-dynamic-types
    icecap-capdl-dynamic-realize
    icecap-capdl-dynamic-realize-simple-config
    icecap-core
  ];
}
