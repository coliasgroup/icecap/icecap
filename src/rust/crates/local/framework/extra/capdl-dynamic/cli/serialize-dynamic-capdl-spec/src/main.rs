#![feature(never_type)]
#![feature(unwrap_infallible)]

use std::{
    env,
    fs::File,
    io::{self, Read, Seek, Write},
    path::Path,
};

use sha2::{Digest, Sha256};

use icecap_capdl_dynamic_types::*;

fn main() -> Result<(), io::Error> {
    let dir = env::args().nth(1).unwrap();
    let dir = Path::new(&dir);
    let spec: Spec<'static, String, FileContent, ()> = serde_json::from_reader(io::stdin())?;
    let dynamic_objects = parse_dynamic_objects(spec);
    let num_nodes = count_nodes(&dynamic_objects);
    let dynamic_spec = DynamicSpec {
        num_nodes,
        objects: dynamic_objects,
    };
    let (dynamic_spec, fill_blob) = add_fill(dynamic_spec, &dir)?;
    io::stdout().write_all(&postcard::to_allocvec(&dynamic_spec).unwrap())?;
    io::stdout().write_all(&fill_blob)?;
    Ok(())
}

fn parse_dynamic_objects(
    spec: Spec<'static, String, FileContent, ()>,
) -> Vec<DynamicNamedObject<DynamicFillForBuildSystem>> {
    spec.objects
        .into_iter()
        .map(
            |NamedObject { name, object }| match name.strip_prefix("extern_") {
                None => DynamicNamedObject {
                    name: name.clone(),
                    object: DynamicObject::Local(object.clone()),
                },
                Some(name) => DynamicNamedObject {
                    name: name.to_owned(),
                    object: DynamicObject::Extern(match object {
                        Object::Endpoint => ExternObject::Endpoint,
                        Object::Notification => ExternObject::Notification,
                        Object::Frame(object::Frame {
                            size_bits,
                            init,
                            paddr,
                        }) => {
                            assert!(paddr.is_none());
                            assert!(init.as_fill().unwrap().is_empty());
                            match size_bits {
                                12 => ExternObject::SmallPage,
                                21 => ExternObject::LargePage,
                                _ => panic!(),
                            }
                        }
                        _ => panic!(),
                    }),
                },
            },
        )
        .collect()
}

fn add_fill(
    spec: DynamicSpec<DynamicFillForBuildSystem>,
    dir: &Path,
) -> Result<(DynamicSpec<DynamicFill>, Vec<u8>), io::Error> {
    let mut fill_blob = vec![];
    let spec = spec.traverse_fill_with_context(|length, orig_content| {
        let path = dir.join(&orig_content.file);
        let mut f = File::open(path)?;
        f.seek(io::SeekFrom::Start(orig_content.file_offset as u64))?;
        let mut content = vec![0; length];
        f.read_exact(&mut content)?;
        let content_digest = {
            let mut hasher = Sha256::new();
            hasher.update(&content);
            hasher.finalize()
        };
        fill_blob.extend(&content);
        Ok::<_, io::Error>(FillEntryContentDigest {
            content_digest: content_digest.to_vec(),
        })
    })?;
    Ok((spec, fill_blob))
}

fn count_nodes<F>(objects: &[DynamicNamedObject<F>]) -> usize {
    objects
        .iter()
        .filter_map(|obj| {
            if let DynamicObject::Local(Object::TCB(object::TCB { extra, .. })) = &obj.object {
                Some(extra.affinity as usize)
            } else {
                None
            }
        })
        .max()
        .map_or(0, |max_affinity| max_affinity + 1)
}
