{ mk, localCrates, postcardCommon, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "icecap-serialize-dynamic-capdl-spec";
  nix.local.dependencies = with localCrates; [
    icecap-capdl-dynamic-types
  ];
  dependencies = {
    inherit (versions) serde ;
    inherit (versions) serde_json ;
    postcard = postcardCommon;
    sha2 = "0.10.6";
  };
}
