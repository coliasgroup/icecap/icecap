{ mk, localCrates, serdeWith }:

mk {
  package.name = "icecap-capdl-dynamic-types";
  nix.local.dependencies = with localCrates; [
    sel4-capdl-initializer-types
  ];
  dependencies = {
    sel4-capdl-initializer-types.features = [ "alloc" "serde" ];
    serde = serdeWith [ "alloc" "derive" ];
  };
  features = {
    sel4 = [ "sel4-capdl-initializer-types/sel4" ];
  };
}
