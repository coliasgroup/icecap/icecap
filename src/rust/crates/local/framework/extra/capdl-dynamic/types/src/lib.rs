#![no_std]

extern crate alloc;

use alloc::string::String;
use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

pub use sel4_capdl_initializer_types::*;

// HACK
pub type ObjectName = String;

pub type DynamicFill = FillEntryContentDigest;
pub type DynamicFillForBuildSystem = FileContent;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct DynamicSpec<D: 'static> {
    pub num_nodes: usize,
    pub objects: Vec<DynamicNamedObject<D>>,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct DynamicNamedObject<D: 'static> {
    pub name: ObjectName,
    pub object: DynamicObject<D>,
}

pub type LocalObject<D> = Object<'static, D, ()>;

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum DynamicObject<D: 'static> {
    Local(LocalObject<D>),
    Extern(ExternObject),
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Serialize, Deserialize)]
pub enum ExternObject {
    Endpoint,
    Notification,
    SmallPage,
    LargePage,
}

impl<D: 'static> DynamicSpec<D> {
    pub fn traverse_fill_with_context<D1, E>(
        &self,
        mut f: impl FnMut(usize, &D) -> Result<D1, E>,
    ) -> Result<DynamicSpec<D1>, E> {
        Ok(DynamicSpec {
            num_nodes: self.num_nodes.clone(),
            objects: self
                .objects
                .iter()
                .map(|obj| obj.traverse_fill_with_context(&mut f))
                .collect::<Result<Vec<DynamicNamedObject<D1>>, E>>()?,
        })
    }
}

impl<D: 'static> DynamicNamedObject<D> {
    pub fn traverse_fill_with_context<D1, E>(
        &self,
        f: impl FnMut(usize, &D) -> Result<D1, E>,
    ) -> Result<DynamicNamedObject<D1>, E> {
        Ok(DynamicNamedObject {
            name: self.name.clone(),
            object: self.object.traverse_fill_with_context(f)?,
        })
    }
}

impl<D: 'static> DynamicObject<D> {
    pub fn traverse_fill_with_context<D1, E>(
        &self,
        mut f: impl FnMut(usize, &D) -> Result<D1, E>,
    ) -> Result<DynamicObject<D1>, E> {
        Ok(match self {
            DynamicObject::Local(obj) => DynamicObject::Local(match obj {
                Object::Untyped(obj) => Object::Untyped(obj.clone()),
                Object::Endpoint => Object::Endpoint,
                Object::Notification => Object::Notification,
                Object::CNode(obj) => Object::CNode(obj.clone()),
                Object::TCB(obj) => Object::TCB(obj.clone()),
                Object::IRQ(obj) => Object::IRQ(obj.clone()),
                Object::VCPU => Object::VCPU,
                Object::Frame(obj) => Object::Frame(object::Frame {
                    size_bits: obj.size_bits,
                    paddr: obj.paddr,
                    init: match &obj.init {
                        FrameInit::Fill(fill) => FrameInit::Fill(Fill {
                            entries: fill
                                .entries
                                .iter()
                                .map(|entry| {
                                    Ok(FillEntry {
                                        range: entry.range.clone(),
                                        content: match &entry.content {
                                            FillEntryContent::BootInfo(content_bootinfo) => {
                                                FillEntryContent::BootInfo(*content_bootinfo)
                                            }
                                            FillEntryContent::Data(content_data) => {
                                                FillEntryContent::Data(f(
                                                    entry.range.len(),
                                                    content_data,
                                                )?)
                                            }
                                        },
                                    })
                                })
                                .collect::<Result<_, E>>()?,
                        }),
                        FrameInit::Embedded(_) => panic!(),
                    },
                }),
                Object::PageTable(obj) => Object::PageTable(obj.clone()),
                Object::ASIDPool(obj) => Object::ASIDPool(obj.clone()),
                Object::ArmIRQ(obj) => Object::ArmIRQ(obj.clone()),
                Object::SchedContext(obj) => Object::SchedContext(obj.clone()),
                Object::Reply => Object::Reply,
            }),
            DynamicObject::Extern(obj) => DynamicObject::Extern(obj.clone()),
        })
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct FillEntryContentDigest {
    pub content_digest: Vec<u8>,
}
