{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-driver-interfaces";
  nix.local.dependencies = with localCrates; [
    icecap-core
  ];
}
