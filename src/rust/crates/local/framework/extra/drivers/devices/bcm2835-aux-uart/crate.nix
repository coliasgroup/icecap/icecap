{ mk, localCrates, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-bcm2835-aux-uart-driver";
  nix.local.dependencies = with localCrates; [
    icecap-core
    icecap-driver-interfaces
  ];
  dependencies = {
    inherit (versions) tock-registers;
  };
}
