{ mk, localCrates, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "crosvm-9p-server-cli";
  nix.local.dependencies = with localCrates; [
    crosvm-9p-server
  ];
  package.edition = "2018";
  dependencies = {
    getopts = "0.2.21";
    libc = "0.2.142";
    inherit (versions) log ;
    env_logger = "0.10.0";
  };
}
