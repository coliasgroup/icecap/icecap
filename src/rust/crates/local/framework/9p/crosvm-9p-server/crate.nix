{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "linux" ];
  package.name = "crosvm-9p-server";
  nix.local.dependencies = with localCrates; [
    crosvm-9p
  ];
  package.edition = "2018";
  dependencies = {
    libc = "0.2.142";
  };
  features.trace = [];
}
