{ mk, versions }:

mk {
  nix.meta.requirements = [ "linux" ];
  package.name = "crosvm-9p-wire-format-derive";
  package.edition = "2018";
  lib.proc-macro = true;
  dependencies = {
    inherit (versions) syn quote proc-macro2;
  };
}
