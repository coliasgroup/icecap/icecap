#![no_std]
#![no_main]

use icecap_fdt::DeviceTree;
use sel4::{BootInfo, BootInfoExtraId};
use sel4_root_task::{debug_println, root_task};

#[root_task(heap_size = 0x4000)]
fn main(bootinfo: &BootInfo) -> ! {
    debug_println!("{:#?}", bootinfo.inner());
    for extra in bootinfo.extra() {
        match extra.id {
            BootInfoExtraId::Fdt => {
                let dt = DeviceTree::read(extra.content()).unwrap();
                debug_println!("{}", dt);
            }
            _ => {}
        }
    }

    BootInfo::init_thread_tcb().tcb_suspend().unwrap();
    unreachable!()
}
