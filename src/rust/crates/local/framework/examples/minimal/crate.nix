{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-minimal";
  nix.local.dependencies = with localCrates; [
    sel4
    sel4-root-task
    icecap-fdt
  ];
  dependencies = {
    sel4-root-task.features = [ "full" ];
  };
}
