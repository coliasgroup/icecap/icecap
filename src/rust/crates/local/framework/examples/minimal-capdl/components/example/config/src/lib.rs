#![no_std]

extern crate alloc;

use alloc::vec::Vec;
use sel4_simple_task_config_types::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub lock_nfn: ConfigCPtr<Notification>,
    pub barrier_nfn: ConfigCPtr<Notification>,
    pub secondary_thread: ConfigCPtr<StaticThread>,
    pub foo: Vec<i32>,
}
