{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "examples-minimal-capdl-component-example-config-cli";
  nix.local.dependencies = with localCrates; [
    icecap-component-config-cli-core
    examples-minimal-capdl-component-example-config
  ];
}
