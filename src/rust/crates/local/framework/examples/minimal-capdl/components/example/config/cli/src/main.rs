use std::io;
use std::marker::PhantomData;

pub fn main() -> Result<(), io::Error> {
    icecap_component_config_cli_core::main(
        PhantomData::<examples_minimal_capdl_component_example_config::Config>,
    )
}
