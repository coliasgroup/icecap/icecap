{ mk, localCrates, serdeWith }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-minimal-capdl-component-example-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
