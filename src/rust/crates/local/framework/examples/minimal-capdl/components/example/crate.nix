{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-minimal-capdl-component-example";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    examples-minimal-capdl-component-example-config
  ];
  dependencies = {
  };
}
