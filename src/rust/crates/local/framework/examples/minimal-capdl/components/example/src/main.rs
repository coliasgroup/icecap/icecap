#![no_std]
#![no_main]

extern crate alloc;

use alloc::sync::Arc;

use icecap_core::prelude::*;
use icecap_core::sync::Mutex;

const INITIAL_VALUE: i32 = 0;

#[sel4_simple_task_runtime::main_postcard]
fn main(config: examples_minimal_capdl_component_example_config::Config) -> Fallible<()> {
    debug_println!("{:#?}", config);

    let lock = Arc::new(Mutex::new(config.lock_nfn.get(), INITIAL_VALUE));
    let barrier_nfn = config.barrier_nfn.get();

    config.secondary_thread.get().start({
        let lock = lock.clone();
        move || {
            {
                let mut value = lock.lock();
                *value += 1;
            }
            debug_println!("secondary thread");
            barrier_nfn.signal();
        }
    });

    {
        let mut value = lock.lock();
        *value += 1;
    }

    barrier_nfn.wait();

    debug_println!("primary thread");

    Ok(())
}
