#![no_std]
#![no_main]

extern crate alloc;

use core::ops::Range;

use serde::{Deserialize, Serialize};

use icecap_capdl_dynamic_realize_simple::{
    fill_frames_simple, initialize_simple_realizer_from_config,
};
use icecap_capdl_dynamic_realize_simple_config::RealizerConfig;
use icecap_capdl_dynamic_types::{DynamicFill, DynamicSpec};
use icecap_core::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Config {
    realizer: RealizerConfig,
    subsystem_spec: Range<usize>,
    nfn: ConfigCPtr<Notification>,
}

#[sel4_simple_task_runtime::main_json]
fn main(config: Config) -> Fallible<()> {
    debug_println!("Hello from supersystem");

    let subsystem_spec_raw = unsafe {
        core::slice::from_raw_parts(
            config.subsystem_spec.start as *const u8,
            config.subsystem_spec.len(),
        )
    };

    let (subsystem_spec, fill_blob): (DynamicSpec<DynamicFill>, &[u8]) =
        postcard::take_from_bytes(&subsystem_spec_raw).unwrap();

    let mut realizer = initialize_simple_realizer_from_config(&config.realizer)?;

    for _ in 0..3 {
        debug_println!("Realizing subsystem");
        let subsystem = {
            let partial_subsystem = realizer.realize_start(subsystem_spec.clone())?;
            fill_frames_simple(&mut realizer, &partial_subsystem, fill_blob)?;
            realizer.realize_finish(partial_subsystem)?
        };

        for (affinity, virtual_core) in subsystem.virtual_cores.iter().enumerate() {
            for virtual_core_tcb in virtual_core.tcbs.iter() {
                let tcb = virtual_core_tcb.cap;
                tcb.tcb_set_affinity(affinity.try_into().unwrap())?;
                if virtual_core_tcb.resume {
                    tcb.tcb_resume()?;
                }
            }
        }

        let _badge = config.nfn.get().wait();

        debug_println!("Destroying subsystem");
        realizer.destroy(subsystem)?;
    }

    Ok(())
}
