#![no_std]
#![no_main]

extern crate alloc;

use serde::{Deserialize, Serialize};

use icecap_core::prelude::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Config {
    nfn: ConfigCPtr<Notification>,
}

#[sel4_simple_task_runtime::main_json]
fn main(config: Config) -> Fallible<()> {
    debug_println!("Hello from subsystem");
    config.nfn.get().signal();
    Ok(())
}
