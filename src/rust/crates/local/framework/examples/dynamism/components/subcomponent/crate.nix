{ mk, localCrates, serdeWith }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-dynamism-component-subcomponent";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
