{ mk, localCrates, serdeWith, postcardWith }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-dynamism-component-supercomponent";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    icecap-capdl-dynamic-types
    icecap-capdl-dynamic-realize
    icecap-capdl-dynamic-realize-simple
    icecap-capdl-dynamic-realize-simple-config
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
    postcard = postcardWith [ "alloc" ];
  };
}
