{ mk, localCrates, serdeWith }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-basic-system-component-serial-server";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    icecap-driver-interfaces
    icecap-pl011-driver
    icecap-ring-buffer
    icecap-ring-buffer-config
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
