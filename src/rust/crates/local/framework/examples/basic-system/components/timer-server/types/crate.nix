{ mk, localCrates, serdeWith }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-basic-system-component-timer-server-types";
  nix.local.dependencies = with localCrates; [
    icecap-core
  ];
  dependencies = {
    icecap-core.features = [ "full" ];
    serde = serdeWith [ "alloc" "derive" ];
  };
}
