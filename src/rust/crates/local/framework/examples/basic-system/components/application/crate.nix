{ mk, localCrates, serdeWith }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "examples-basic-system-component-application";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    icecap-ring-buffer
    icecap-ring-buffer-config
    examples-basic-system-component-timer-server-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
