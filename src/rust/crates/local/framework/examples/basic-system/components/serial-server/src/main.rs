#![no_std]
#![no_main]

extern crate alloc;

use serde::{Deserialize, Serialize};

use icecap_core::component_config_types::*;
use icecap_core::prelude::*;
use icecap_driver_interfaces::SerialDevice;
use icecap_pl011_driver::Pl011Device;
use icecap_ring_buffer::BufferedRingBuffer;
use icecap_ring_buffer_config::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Config {
    dev_vaddr: usize,
    event_nfn: ConfigCPtr<Notification>,
    irq_handler: ConfigCPtr<IRQHandler>,
    client_ring_buffer: UnmanagedRingBufferConfig,
    badges: Badges,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Badges {
    irq: Badge,
    client: Badge,
}

#[sel4_simple_task_runtime::main_json]
fn main(config: Config) -> Fallible<()> {
    let dev = Pl011Device::new(config.dev_vaddr);
    dev.init();
    config.irq_handler.get().irq_handler_ack()?;

    let mut rb = BufferedRingBuffer::new(config.client_ring_buffer.realize());
    rb.ring_buffer().enable_notify_read();
    rb.ring_buffer().enable_notify_write();

    loop {
        let ((), badge) = config.event_nfn.get().wait();

        if badge & config.badges.irq != 0 {
            while let Some(c) = dev.get_char() {
                rb.tx(&[c]);
            }
            dev.handle_interrupt();
            config.irq_handler.get().irq_handler_ack()?;
        }

        if badge & config.badges.client != 0 {
            rb.rx_callback();
            rb.tx_callback();
            if let Some(chars) = rb.rx() {
                for c in chars {
                    dev.put_char(c);
                }
            }
            rb.ring_buffer().enable_notify_read();
            rb.ring_buffer().enable_notify_write();
        }
    }
}
