#![no_main]

use icecap_core::failure::Fallible;

#[sel4_simple_task_runtime::main_json]
fn main(_: ()) -> Fallible<()> {
    icecap_core_external::early_init();
    // let n: libc::size_t = 100usize; // TODO (see crate.nix)
    let n = 100usize;
    let r = 0..n;
    let mut v = vec![];
    for i in r.clone() {
        v.push(i);
    }
    assert_eq!(v.iter().sum::<usize>(), r.sum::<usize>());
    println!("Hello, println!");
    println!("TEST_PASS");
    Ok(())
}
