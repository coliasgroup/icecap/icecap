{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "tests-capdl-sysroot-test";
  nix.local.dependencies = with localCrates; [
    icecap-core
    icecap-std-external
  ];
  dependencies = {
    icecap-core.features = [ "alloc" ];
    # TODO use this to test this method of specifying deps
    # libc = localCrates._patches.libc.dep;
  };
  nix.meta.skip = true;
}
