{ mk, localCrates, postcardWith, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-failure";
  nix.local.dependencies = with localCrates; [
    icecap-failure-derive
    sel4-backtrace
    sel4
    sel4-simple-task-rpc
  ];
  package.edition = "2018";
  dependencies = {
    sel4-backtrace.features = [ "alloc" ];
    inherit (versions) cfg-if ;
    postcard = postcardWith [] // { optional = true; };
  };
  features = {
    postcard = [
      "sel4-backtrace/postcard"
      "sel4-simple-task-rpc/postcard"
      "dep:postcard"
    ];
  };
}
