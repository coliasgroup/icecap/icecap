use alloc::{ffi, str, string};
use core::alloc::{AllocError, LayoutError};
use core::{array, cell, char, fmt, num};

use crate::Fail;

impl Fail for sel4::Error {}

#[cfg(feature = "postcard")]
impl Fail for sel4_simple_task_rpc::easy::Error {}

// HACK trivial impls to match std/error.rs

impl Fail for ! {}

impl Fail for AllocError {}

impl Fail for LayoutError {}

impl Fail for str::ParseBoolError {}

impl Fail for str::Utf8Error {}

impl Fail for num::ParseIntError {}

impl Fail for num::TryFromIntError {}

impl Fail for array::TryFromSliceError {}

impl Fail for num::ParseFloatError {}

impl Fail for string::FromUtf8Error {}

impl Fail for string::FromUtf16Error {}

impl Fail for string::ParseError {}

impl Fail for char::DecodeUtf16Error {}

impl Fail for fmt::Error {}

impl Fail for cell::BorrowError {}

impl Fail for cell::BorrowMutError {}

impl Fail for char::CharTryFromError {}

impl Fail for char::ParseCharError {}

impl Fail for ffi::NulError {}
