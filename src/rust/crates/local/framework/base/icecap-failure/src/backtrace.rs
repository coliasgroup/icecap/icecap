use sel4_backtrace::Backtrace as InnerBacktrace;

#[derive(Debug, Clone)]
pub struct Backtrace {
    pub inner: Option<InnerBacktrace<&'static str>>,
}

impl Backtrace {
    pub fn new() -> Self {
        Backtrace {
            inner: if is_backtrace_enabled() {
                // TODO
                // Some(collect())
                None
            } else {
                None
            },
        }
    }

    pub fn none() -> Self {
        Backtrace { inner: None }
    }

    #[allow(dead_code)]
    pub(crate) fn is_none(&self) -> bool {
        self.inner.is_none()
    }

    /// Returns true if displaying this backtrace would be an empty string.
    pub fn is_empty(&self) -> bool {
        self.inner.is_none()
    }
}

fn is_backtrace_enabled() -> bool {
    // TODO
    cfg_if::cfg_if! {
        if #[cfg(debug_assertions)] {
            true
        } else {
            false
        }
    }
}
