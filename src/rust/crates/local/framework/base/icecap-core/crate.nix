{ mk, localCrates }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "icecap-core";
  nix.local.dependencies = with localCrates; [
    sel4
    sel4-sync
    sel4-logging
    sel4-panicking
    sel4-backtrace
    sel4-panicking-env
    sel4-simple-task-rpc
    sel4-simple-task-threading
    icecap-failure
    sel4-simple-task-config-types
  ];
  dependencies = {
    sel4-panicking.features = [ "unwinding" ];
    sel4-backtrace.features = [ "postcard" "unwinding" ];
    sel4-simple-task-rpc.features = [ "postcard" ];
    icecap-failure = { features = [ "postcard" ]; optional = true; };
  };
  features = {
    default = [
      "alloc"
    ];
    alloc = [
      "sel4-panicking/alloc"
      "sel4-backtrace/alloc"
      "sel4-simple-task-threading/alloc"
      "dep:icecap-failure"
    ];
    full = [
      "alloc"
    ];
  };
}
