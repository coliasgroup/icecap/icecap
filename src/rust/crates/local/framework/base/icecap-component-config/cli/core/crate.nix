{ mk, postcardCommon, versions }:

mk {
  nix.meta.requirements = [ "linux" ];
  package.name = "icecap-component-config-cli-core";
  dependencies = {
    inherit (versions) serde ;
    inherit (versions) serde_json ;
    postcard = postcardCommon;
  };
}
