{ mk, serdeWith, localCrates, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-tests-capdl-benchmark-server-test";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    hypervisor-benchmark-server-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
    cortex-a = "8.1.1";
    tock-registers = { version = versions.tock-registers; default-features = false; };
  };
}
