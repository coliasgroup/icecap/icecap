#![no_std]
#![no_main]

extern crate alloc;

use cortex_a::registers::{CNTFRQ_EL0, CNTVCT_EL0};
use serde::{Deserialize, Serialize};
use tock_registers::interfaces::Readable;

use hypervisor_benchmark_server_types as benchmark_server;
use icecap_core::prelude::*;
use icecap_core::rpc::easy as rpc;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Config {
    benchmark_server_ep: ConfigCPtr<Endpoint>,
}

sel4_cfg_if! {
    if #[cfg(PLAT_QEMU_ARM_VIRT)] {
        const C: u32 = 10;
    } else if #[cfg(PLAT_BCM2711)] {
        const C: u32 = 1;
    }
}

#[sel4_simple_task_runtime::main_json]
fn main(config: Config) -> Fallible<()> {
    let ep = config.benchmark_server_ep.get();

    let freq = read_cntfrq_el0();
    debug_println!("freq = {}", freq);

    let start = read_cntvct_el0();
    debug_println!("start = {}", start);

    let client = rpc::Client::new(ep);

    client
        .call::<benchmark_server::Response>(&benchmark_server::Request::Start)?
        .unwrap();

    for _ in 0..6 {
        loop {
            let t = read_cntvct_el0();
            if t % ((freq / C) as u64) == 0u64 {
                debug_println!("t = {}", t);
                break;
            }
            // sel4::yield_();
        }
    }

    client
        .call::<benchmark_server::Response>(&benchmark_server::Request::Finish)?
        .unwrap();

    Ok(())
}

#[inline(never)]
pub fn read_cntfrq_el0() -> u32 {
    CNTFRQ_EL0.get() as u32
}

#[inline(never)]
pub fn read_cntvct_el0() -> u64 {
    CNTVCT_EL0.get()
}
