{ mk, localCrates, postcardCommon, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "hypervisor-serialize-component-config";
  nix.local.dependencies = with localCrates; [
    icecap-component-config-cli-core

    hypervisor-fault-handler-config
    hypervisor-serial-server-config
    hypervisor-host-vmm-config
    hypervisor-realm-vmm-config
    hypervisor-resource-server-config
    hypervisor-event-server-config
    hypervisor-benchmark-server-config
    hypervisor-mirage-config
  ];
  dependencies = {
    inherit (versions) serde ;
    inherit (versions) serde_json ;
    postcard = postcardCommon;
  };
}
