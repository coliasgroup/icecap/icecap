{ mk, localCrates, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "hypervisor-fdt-append-devices";
  nix.local.dependencies = with localCrates; [
    icecap-fdt
    icecap-vmm-fdt-bindings
  ];
  dependencies = {
    inherit (versions) serde ;
    inherit (versions) serde_json ;
  };
}
