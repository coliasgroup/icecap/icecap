{ mk, localCrates, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "hypervisor-serialize-event-server-out-index";
  nix.local.dependencies = with localCrates; [
    hypervisor-event-server-types
    finite-set
  ];
  dependencies = {
    inherit (versions) serde ;
    inherit (versions) serde_json ;
  };
}
