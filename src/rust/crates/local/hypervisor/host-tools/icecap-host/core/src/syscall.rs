use std::fs::File;
use std::mem;
use std::os::unix::io::AsRawFd;
use std::path::Path;

use serde::{Deserialize, Serialize};

use hypervisor_host_vmm_types::{sys_id as host_vmm_sys_id, DirectRequest, DirectResponse};
use hypervisor_resource_server_types::Request;

cfg_if::cfg_if! {
    if #[cfg(target_env = "gnu")] {
        type Ioctl = u64;
    } else if #[cfg(target_env = "musl")] {
        type Ioctl = i32;
    }
}

const ICECAP_VMM_PASSTHRU: u32 = 0xc0403300;
const ICECAP_RESOURCE_SERVER_YIELD_TO: u32 = 0xc0103301;

const ICECAP_VMM_IOCTL_PATH: &'static str = "/sys/kernel/debug/icecap_vmm";
const ICECAP_RESOURCE_SERVER_IOCTL_PATH: &'static str = "/dev/icecap_resource_server";

#[repr(C)]
struct Passthru {
    sys_id: u64,
    regs: [u64; 7],
}

#[repr(C)]
struct YieldTo {
    realm_id: u64,
    virtual_node: u64,
}

fn ioctl<T>(path: impl AsRef<Path>, request: u32, ptr: *mut T) {
    let f = File::open(path).unwrap();
    let ret = unsafe { libc::ioctl(f.as_raw_fd(), request as Ioctl, ptr) };
    assert_eq!(ret, 0);
}

fn ioctl_passthru(passthru: &mut Passthru) {
    ioctl(
        ICECAP_VMM_IOCTL_PATH,
        ICECAP_VMM_PASSTHRU,
        passthru as *mut Passthru,
    )
}

fn ioctl_yield_to(yield_to: &mut YieldTo) {
    ioctl(
        ICECAP_RESOURCE_SERVER_IOCTL_PATH,
        ICECAP_RESOURCE_SERVER_YIELD_TO,
        yield_to as *mut YieldTo,
    )
}

//

fn call_passthru<T: Serialize, U: for<'a> Deserialize<'a>>(sys_id: u64, input: &T) -> U {
    let mut in_bytes = postcard::to_allocvec(input).unwrap();
    let in_len_words = round_up(in_bytes.len(), mem::size_of::<u64>()) / mem::size_of::<u64>();
    assert!(in_len_words <= 6);
    in_bytes.resize_with(in_len_words * mem::size_of::<u64>(), || 0);
    let mut in_words = vec![];
    for i in 0..in_len_words {
        in_words.push(u64::from_ne_bytes(
            <[u8; mem::size_of::<u64>()]>::try_from(
                &in_bytes[mem::size_of::<u64>() * i..][..mem::size_of::<u64>()],
            )
            .unwrap(),
        ));
    }
    in_words.resize_with(6, || 0);
    let mut passthru = Passthru {
        sys_id,
        regs: [0; 7],
    };
    passthru.regs[0] = in_len_words as u64;
    passthru.regs[1..].copy_from_slice(&in_words);
    ioctl_passthru(&mut passthru);
    let out_words = &passthru.regs[1..][..passthru.regs[0] as usize];
    let mut out_bytes = vec![];
    for w in out_words.iter() {
        out_bytes.extend(w.to_ne_bytes());
    }
    postcard::from_bytes(&out_bytes).unwrap()
}

pub fn direct(request: &DirectRequest) -> DirectResponse {
    call_passthru(host_vmm_sys_id::DIRECT, request)
}

fn resource_server_passthru<Output: for<'a> Deserialize<'a>>(request: &Request) -> Output {
    call_passthru(host_vmm_sys_id::RESOURCE_SERVER_PASSTHRU, request)
}

//

pub fn declare(realm_id: usize, spec_size: usize) {
    resource_server_passthru(&Request::Declare {
        realm_id,
        spec_size,
    })
}

pub fn spec_chunk(realm_id: usize, bulk_data_offset: usize, bulk_data_size: usize, offset: usize) {
    resource_server_passthru(&Request::SpecChunk {
        realm_id,
        bulk_data_offset,
        bulk_data_size,
        offset,
    })
}

pub fn fill_chunks(realm_id: usize, bulk_data_offset: usize, bulk_data_size: usize) {
    resource_server_passthru(&Request::FillChunks {
        realm_id,
        bulk_data_offset,
        bulk_data_size,
    })
}

pub fn realize_start(realm_id: usize) {
    resource_server_passthru(&Request::RealizeStart { realm_id })
}

pub fn realize_finish(realm_id: usize) {
    resource_server_passthru(&Request::RealizeFinish { realm_id })
}

pub fn destroy(realm_id: usize) {
    resource_server_passthru(&Request::Destroy { realm_id })
}

pub fn hack_run(realm_id: usize) {
    resource_server_passthru(&Request::HackRun { realm_id })
}

//

pub fn yield_to(realm_id: usize, virtual_node: usize) {
    let mut yield_to = YieldTo {
        realm_id: realm_id as u64,
        virtual_node: virtual_node as u64,
    };
    ioctl_yield_to(&mut yield_to);
}

//

const fn round_up(n: usize, b: usize) -> usize {
    let r = n % b;
    n + if r == 0 { 0 } else { b - r }
}
