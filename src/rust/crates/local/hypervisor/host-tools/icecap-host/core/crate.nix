{ mk, localCrates, serdeWith, postcardCommon, versions }:

mk {
  nix.meta.requirements = [ "linux" ];
  package.name = "icecap-host-core";
  nix.local.dependencies = with localCrates; [
    icecap-capdl-dynamic-types
    hypervisor-host-vmm-types
    hypervisor-resource-server-types
  ];
  dependencies = {
    libc = "0.2.142";
    inherit (versions) cfg-if ;
    serde = serdeWith [ "alloc" ];
    postcard = postcardCommon;
  };
}
