{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "linux" ];
  package.name = "icecap-host";
  nix.local.dependencies = with localCrates; [
    icecap-host-core
    hypervisor-host-vmm-types
  ];
  dependencies = {
    clap = "2.34.0";
  };
}
