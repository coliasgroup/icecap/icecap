#![no_std]
#![no_main]

extern crate alloc;

use serde::{Deserialize, Serialize};

use icecap_core::prelude::*;
use icecap_ring_buffer_config::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    con: UnmanagedRingBufferConfig,
}

#[sel4_simple_task_runtime::main_json]
fn main(config: Config) -> Fallible<()> {
    let mut con = config.con.realize_resuming();
    con.write(b"Hello, World!");
    con.notify_write();
    Ok(())
}
