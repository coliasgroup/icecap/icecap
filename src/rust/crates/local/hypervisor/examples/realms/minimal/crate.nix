{ mk, localCrates, serdeWith }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-examples-realms-minimal";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    icecap-ring-buffer-config
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
