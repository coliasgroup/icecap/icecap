{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-fault-handler";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    hypervisor-fault-handler-config
  ];
  dependencies = {
  };
}
