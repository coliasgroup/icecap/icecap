#![no_std]

extern crate alloc;

use alloc::collections::btree_map::BTreeMap;
use alloc::string::String;

use serde::{Deserialize, Serialize};

use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub ep: ConfigCPtr<Endpoint>,
    pub threads: BTreeMap<ConfigBadge, Thread>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Thread {
    pub name: String,
    pub tcb: ConfigCPtr<TCB>,
}
