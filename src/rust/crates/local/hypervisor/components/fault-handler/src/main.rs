#![no_std]
#![no_main]

extern crate alloc;

use hypervisor_fault_handler_config::{Config, Thread};
use icecap_core::prelude::*;
use icecap_core::sel4::Fault;

#[sel4_simple_task_runtime::main_postcard]
fn main(config: Config) -> Fallible<()> {
    let ep = config.ep.get();
    loop {
        let (tag, badge) = ep.recv(());
        let fault = sel4::with_borrow_ipc_buffer(|ipc_buffer| Fault::new(ipc_buffer, &tag));
        handle(&config.threads[&badge.into()], &fault)?;
    }
}

fn handle(thread: &Thread, fault: &Fault) -> Fallible<()> {
    debug_println!("fault from {:?}: {:x?}", thread.name, fault);
    Ok(())
}
