#![no_std]

extern crate alloc;

use alloc::collections::BTreeMap;
use alloc::vec::Vec;

use hypervisor_event_server_types::*;
use sel4_simple_task_config_types::*;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub lock: ConfigCPtr<Notification>,

    pub endpoints: Vec<ConfigCPtr<Endpoint>>,
    pub secondary_threads: Vec<ConfigCPtr<StaticThread>>,

    pub badges: Badges,

    pub host_notifications: Vec<ClientNodeConfig>,
    pub realm_notifications: Vec<Vec<ClientNodeConfig>>,
    pub resource_server_subscriptions: Vec<ConfigCPtr<Notification>>,

    pub irqs: BTreeMap<usize, (ConfigCPtr<IRQHandler>, Vec<ConfigCPtr<Notification>>)>,
    pub irq_threads: Vec<IRQThreadConfig>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ClientNodeConfig {
    pub nfn: Vec<ConfigCPtr<Notification>>,
    pub bitfield: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Badges {
    pub client_badges: Vec<ClientId>,
    pub resource_server_badge: ConfigBadge,
    pub host_badge: ConfigBadge,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct IRQThreadConfig {
    pub thread: ConfigCPtr<StaticThread>,
    pub notification: ConfigCPtr<Notification>,
    pub irqs: Vec<usize>, // per bit
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ClientId {
    ResourceServer,
    SerialServer,
    Host,
    Realm(RealmId),
}
