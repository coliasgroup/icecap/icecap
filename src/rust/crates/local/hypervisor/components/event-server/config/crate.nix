{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-event-server-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    hypervisor-event-server-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
