#![no_std]
#![no_main]
#![feature(never_type)]
#![allow(unused_variables)]

extern crate alloc;

use alloc::sync::Arc;

use hypervisor_event_server_config::*;
use hypervisor_event_server_types::*;
use icecap_core::prelude::*;
use icecap_core::rpc::easy as rpc;
use icecap_core::sync::*;

mod server;

use server::*;

const BADGE_TYPE_MASK: Badge = 3 << 11;
const BADGE_TYPE_CLIENT: Badge = 3;
const BADGE_TYPE_CLIENT_OUT: Badge = 2;
const BADGE_TYPE_CONTROL: Badge = 1;

#[sel4_simple_task_runtime::main_postcard]
pub fn main(config: Config) -> Fallible<()> {
    let server = EventServerConfig {
        host_notifications: config.host_notifications,
        realm_notifications: config.realm_notifications,
        resource_server_subscriptions: config.resource_server_subscriptions,
        irqs: config.irqs,
    }
    .realize();
    let server = Arc::new(Mutex::new(config.lock.get(), server));

    for irq_thread_config in &config.irq_threads {
        irq_thread_config.thread.get().start({
            let irq_thread = IRQThread {
                notification: irq_thread_config.notification.get(),
                irqs: irq_thread_config.irqs.clone(),
                server: server.clone(),
            };
            move || irq_thread.run().unwrap()
        })
    }

    let badges = Arc::new(config.badges);

    for (endpoint, thread) in config
        .endpoints
        .iter()
        .skip(1)
        .zip(&config.secondary_threads)
    {
        thread.get().start({
            let server = server.clone();
            let badges = badges.clone();
            let endpoint = endpoint.get();
            move || run(&server, endpoint, &badges).unwrap()
        })
    }

    run(&server, config.endpoints[0].get(), &badges)?
}

fn run(server: &Mutex<EventServer>, endpoint: Endpoint, badges: &Badges) -> Fallible<!> {
    loop {
        enum Received {
            ClientOut {
                out_index: usize,
                client_badge: ClientId,
            },
            Client {
                req: calls::Client,
                client_badge: ClientId,
            },
            ResourceServer(calls::ResourceServer),
            Host(calls::Host),
        }

        let received = rpc::server::recv(endpoint, |reception| {
            let badge_type = reception.badge() >> 11;
            let badge_value = reception.badge() & !BADGE_TYPE_MASK;
            match badge_type {
                BADGE_TYPE_CLIENT_OUT => Received::ClientOut {
                    out_index: sel4::with_borrow_ipc_buffer(|ipc_buffer| {
                        ipc_buffer.msg_regs()[0] as usize
                    }),
                    client_badge: badges.client_badges[badge_value as usize].clone(),
                },
                BADGE_TYPE_CLIENT => Received::Client {
                    req: reception.read().unwrap(),
                    client_badge: badges.client_badges[badge_value as usize].clone(),
                },
                BADGE_TYPE_CONTROL if badge_value == badges.resource_server_badge.get() => {
                    Received::ResourceServer(reception.read().unwrap())
                }
                BADGE_TYPE_CONTROL if badge_value == badges.resource_server_badge.get() => {
                    Received::Host(reception.read().unwrap())
                }
                _ => panic!(),
            }
        });

        let mut server = server.lock();
        match received {
            Received::ClientOut {
                out_index,
                client_badge,
            } => {
                // debug_println!("out: {:?} {:?}", out_index, client_badge);
                let client = match client_badge {
                    ClientId::ResourceServer => &mut server.resource_server,
                    ClientId::SerialServer => &mut server.serial_server,
                    ClientId::Host => &mut server.host,
                    ClientId::Realm(rid) => server.realms.get_mut(&rid).unwrap(),
                };
                let () = client.signal(out_index)?;
                sel4::with_borrow_ipc_buffer_mut(|ipc_buffer| {
                    sel4::reply(ipc_buffer, MessageInfo::new(0, 0, 0, 0))
                });
            }
            Received::Client { req, client_badge } => {
                let client = match client_badge {
                    ClientId::ResourceServer => &mut server.resource_server,
                    ClientId::SerialServer => &mut server.serial_server,
                    ClientId::Host => &mut server.host,
                    ClientId::Realm(rid) => server.realms.get_mut(&rid).unwrap(),
                };
                match req {
                    calls::Client::Signal { index } => {
                        rpc::server::reply(&client.signal(index)?)?;
                    }
                    calls::Client::SEV { nid } => {
                        rpc::server::reply(&client.sev(nid)?)?;
                    }
                    calls::Client::Poll { nid } => {
                        rpc::server::reply(&client.poll(nid)?)?;
                    }
                    calls::Client::End { nid, index } => {
                        rpc::server::reply(&client.end(nid, index)?)?;
                    }
                    calls::Client::Configure { nid, index, action } => {
                        rpc::server::reply(&client.configure(nid, index, action)?)?;
                    }
                    calls::Client::Move {
                        src_nid,
                        src_index,
                        dst_nid,
                        dst_index,
                    } => {
                        rpc::server::reply(&client.move_(src_nid, src_index, dst_nid, dst_index)?)?;
                    }
                }
            }
            Received::ResourceServer(req) => match req {
                calls::ResourceServer::Subscribe { nid, host_nid } => {
                    rpc::server::reply(&server.resource_server_subscribe(nid, host_nid)?)?;
                }
                calls::ResourceServer::Unsubscribe { nid, host_nid } => {
                    rpc::server::reply(&server.resource_server_unsubscribe(nid, host_nid)?)?;
                }
                calls::ResourceServer::CreateRealm {
                    realm_id,
                    num_nodes,
                } => {
                    rpc::server::reply(&server.create_realm(realm_id, num_nodes)?)?;
                }
                calls::ResourceServer::DestroyRealm { realm_id } => {
                    rpc::server::reply(&server.destroy_realm(realm_id)?)?;
                }
            },
            Received::Host(req) => match req {
                calls::Host::Subscribe {
                    nid,
                    realm_id,
                    realm_nid,
                } => {
                    rpc::server::reply(&server.host_subscribe(nid, realm_id, realm_nid)?)?;
                }
            },
        }
    }
}
