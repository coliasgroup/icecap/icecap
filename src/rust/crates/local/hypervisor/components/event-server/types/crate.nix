{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-event-server-types";
  nix.local.dependencies = with localCrates; [
    biterate
    finite-set
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
