{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-event-server";
  nix.local.dependencies = with localCrates; [
    biterate
    finite-set
    icecap-core
    sel4-simple-task-runtime
    hypervisor-event-server-types
    hypervisor-event-server-config
  ];
  dependencies = {
  };
}
