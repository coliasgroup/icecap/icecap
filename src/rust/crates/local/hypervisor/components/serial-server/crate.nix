{ mk, localCrates, serdeWith, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-serial-server";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    hypervisor-serial-server-config
    icecap-generic-timer-server-client
    icecap-generic-serial-server-core
    hypervisor-event-server-types
    icecap-driver-interfaces
    finite-set
    icecap-ring-buffer
    icecap-ring-buffer-config

    # TODO see note in timer-server/crate.nix
    icecap-pl011-driver
    icecap-bcm2835-aux-uart-driver
  ];
  dependencies = {
    inherit (versions) cfg-if ;
    inherit (versions) tock-registers;
    serde = serdeWith [ "alloc" "derive" ];
  };
}
