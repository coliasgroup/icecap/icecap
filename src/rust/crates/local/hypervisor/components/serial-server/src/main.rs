#![no_std]
#![no_main]

extern crate alloc;

use alloc::boxed::Box;
use alloc::vec;

use finite_set::Finite;
use hypervisor_event_server_types::calls::Client as EventServerRequest;
use hypervisor_event_server_types::events;
use hypervisor_serial_server_config::Config;
use icecap_core::prelude::*;
use icecap_core::rpc::easy as rpc;
use icecap_generic_timer_server_client::*;
use icecap_ring_buffer::Kick;
use icecap_ring_buffer_config::RingBufferKicksConfig;

use icecap_generic_serial_server_core::{plat_init_device, run, ClientId, Event};

#[sel4_simple_task_runtime::main_postcard]
pub fn main(config: Config) -> Fallible<()> {
    let timer = TimerClient::new(config.timer_ep_write.get());

    let event_server = rpc::Client::<EventServerRequest>::new(config.event_server.get());
    let mk_signal = move |index: events::SerialServerOut| -> Kick {
        let event_server = event_server.clone();
        let index = index.to_nat();
        Box::new(move || {
            event_server
                .call::<()>(&EventServerRequest::Signal { index })
                .unwrap()
        })
    };
    let mk_kicks = |rb: events::SerialServerRingBuffer| RingBufferKicksConfig {
        read: mk_signal(events::SerialServerOut::RingBuffer(rb.clone())),
        write: mk_signal(events::SerialServerOut::RingBuffer(rb.clone())),
    };

    let mut clients = vec![];
    clients.push(
        config
            .host_client
            .ring_buffer
            .realize(mk_kicks(events::SerialServerRingBuffer::Host)),
    );
    for (i, realm) in config.realm_clients.iter().enumerate() {
        clients.push(
            realm
                .ring_buffer
                .realize(mk_kicks(events::SerialServerRingBuffer::Realm(
                    events::RealmId(i),
                ))),
        );
    }

    let event_ep = config.ep.get();

    let cspace = config.cnode.get();
    let reply_ep = config.reply_ep.get();

    let dev = plat_init_device(config.dev_vaddr);

    let irq_nfn = config.irq_nfn.get();
    let irq_handler = config.irq_handler.get();
    config.irq_thread.get().start(move || loop {
        irq_handler.irq_handler_ack().unwrap();
        irq_nfn.wait();
        rpc::Client::<Event>::new(event_ep)
            .call::<()>(&Event::Interrupt)
            .unwrap();
    });

    let timer_wait = config.timer_wait.get();
    config.timer_thread.get().start(move || loop {
        timer_wait.wait();
        rpc::Client::<Event>::new(event_ep)
            .call::<()>(&Event::Timeout)
            .unwrap();
    });

    for (i, client) in core::iter::once(&config.host_client)
        .chain(config.realm_clients.iter())
        .enumerate()
    {
        let nfn = client.wait.get();
        client.thread.get().start(move || loop {
            nfn.wait();
            rpc::Client::<Event>::new(event_ep)
                .call::<()>(&Event::Con(i as ClientId))
                .unwrap();
        });
    }

    run(clients, timer, event_ep, cspace, reply_ep, dev);
    Ok(())
}
