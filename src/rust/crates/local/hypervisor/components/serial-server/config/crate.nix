{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-serial-server-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    icecap-ring-buffer-config
    hypervisor-event-server-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
