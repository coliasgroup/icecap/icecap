#![no_std]

extern crate alloc;

use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use icecap_ring_buffer_config::*;
use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub cnode: ConfigCPtr<CNode>,
    pub reply_ep: ConfigCPtr<Endpoint>,
    pub dev_vaddr: usize,
    pub ep: ConfigCPtr<Endpoint>,
    pub event_server: ConfigCPtr<Endpoint>,
    pub host_client: Client,
    pub realm_clients: Vec<Client>,
    pub irq_nfn: ConfigCPtr<Notification>,
    pub irq_handler: ConfigCPtr<IRQHandler>,
    pub irq_thread: ConfigCPtr<StaticThread>,
    pub timer_ep_write: ConfigCPtr<Endpoint>,
    pub timer_wait: ConfigCPtr<Notification>,
    pub timer_thread: ConfigCPtr<StaticThread>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Client {
    pub thread: ConfigCPtr<StaticThread>,
    pub wait: ConfigCPtr<Notification>,
    pub ring_buffer: RingBufferConfig,
}
