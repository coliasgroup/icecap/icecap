{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-idle";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
  ];
  dependencies = {
    cortex-a = "8.1.1";
  };
}
