#![no_std]
#![no_main]

use cortex_a::asm::wfi;

#[sel4_simple_task_runtime::main]
fn main(_: &[u8]) {
    loop {
        wfi()
    }
}
