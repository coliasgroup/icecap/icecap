{ mk, localCrates, serdeWith, postcardCommon }:

mk {
  package.name = "hypervisor-resource-server-types";
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
    postcard = postcardCommon;
  };
}
