{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-resource-server-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    icecap-capdl-dynamic-types
    icecap-capdl-dynamic-realize-simple-config
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
