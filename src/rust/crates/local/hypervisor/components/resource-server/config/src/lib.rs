#![no_std]

extern crate alloc;

use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use icecap_capdl_dynamic_realize_simple_config::*;
use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub lock: ConfigCPtr<Notification>,

    pub realizer: RealizerConfig,

    pub host_bulk_region_start: usize,
    pub host_bulk_region_size: usize,

    pub cnode: ConfigCPtr<CNode>,
    pub local: Vec<Local>,
    pub secondary_threads: Vec<ConfigCPtr<StaticThread>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Local {
    pub endpoint: ConfigCPtr<Endpoint>,
    pub reply_slot: ConfigCPtr<Endpoint>,
    pub timer_server_client: ConfigCPtr<Endpoint>,
    pub event_server_client: ConfigCPtr<Endpoint>,
    pub event_server_control: ConfigCPtr<Endpoint>,
}
