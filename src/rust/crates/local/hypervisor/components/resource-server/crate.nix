{ mk, localCrates, serdeWith, postcardCommon }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-resource-server";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    hypervisor-event-server-types
    hypervisor-resource-server-types
    hypervisor-resource-server-config
    hypervisor-resource-server-core
    icecap-generic-timer-server-client
    icecap-capdl-dynamic-types
    icecap-capdl-dynamic-realize
    icecap-capdl-dynamic-realize-simple
    icecap-capdl-dynamic-realize-simple-config
  ];
  dependencies = {
    postcard = postcardCommon;
    serde = serdeWith [ "alloc" "derive" ];
  };
}
