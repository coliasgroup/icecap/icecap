use icecap_core::prelude::*;

pub const NUM_ACTIVE_CORES: usize = sel4::sel4_cfg_usize!(MAX_NUM_NODES) - 1;

pub fn schedule(tcb: TCB, node: Option<usize>) -> sel4::Result<()> {
    tcb.tcb_set_affinity(node.unwrap_or(NUM_ACTIVE_CORES) as u64)
}
