{ mk, localCrates, serdeWith, postcardCommon, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-resource-server-core";
  nix.local.dependencies = with localCrates; [
    icecap-core
    icecap-capdl-dynamic-types
    icecap-capdl-dynamic-realize
    hypervisor-resource-server-types
    hypervisor-event-server-types
    icecap-generic-timer-server-client
  ];
  dependencies = {
    icecap-core.features = [ "full" ];
    inherit (versions) log ;
    serde = serdeWith [ "alloc" "derive" ];
    postcard = postcardCommon;
  };
}
