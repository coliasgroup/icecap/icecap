{ mk, serdeWith, localCrates }:

mk {
  package.name = "hypervisor-host-vmm-types";
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
