#![no_std]

extern crate alloc;

use alloc::collections::BTreeMap;
use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use hypervisor_event_server_types::events::HostIn;
use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub cnode: ConfigCPtr<CNode>,
    pub gic_lock: ConfigCPtr<Notification>,
    pub nodes_lock: ConfigCPtr<Notification>,
    pub gic_dist_paddr: usize,
    pub nodes: Vec<Node>,
    pub event_server_client_ep: Vec<ConfigCPtr<Endpoint>>,
    pub event_server_control_ep: Vec<ConfigCPtr<Endpoint>>,
    pub resource_server_ep: Vec<ConfigCPtr<Endpoint>>,
    pub benchmark_server_ep: ConfigCPtr<Endpoint>,

    pub ppi_map: BTreeMap<usize, (HostIn, bool)>, // in_index, must_ack
    pub spi_map: BTreeMap<usize, (HostIn, usize, bool)>, // in_index, nid, must_ack

    pub log_buffer: ConfigCPtr<LargePage>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    pub tcb: ConfigCPtr<TCB>,
    pub vcpu: ConfigCPtr<VCPU>,
    pub thread: ConfigCPtr<StaticThread>,
    pub ep_read: ConfigCPtr<Endpoint>,
    pub fault_reply_slot: ConfigCPtr<Endpoint>,
    pub event_server_bitfield: usize,
}
