{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-host-vmm-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    hypervisor-event-server-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
