#![no_std]
#![no_main]

extern crate alloc;

use alloc::vec;
use alloc::vec::Vec;
use core::mem;

use finite_set::Finite;
use hypervisor_host_vmm_config::*;
use hypervisor_host_vmm_types::{sys_id, DirectRequest, DirectResponse};
use hypervisor_vmm_core::*;
use icecap_core::prelude::*;
use icecap_core::rpc::easy as rpc;
use icecap_core::sel4::UnknownSyscall;
use sel4_supervising::UnknownSyscallExt;

#[allow(unused_imports)]
use hypervisor_benchmark_server_types as benchmark_server;

#[sel4_simple_task_runtime::main_postcard]
pub fn main(config: Config) -> Fallible<()> {
    let resource_server_ep = config.resource_server_ep;
    let event_server_client_ep = config
        .event_server_client_ep
        .iter()
        .map(ConfigCPtr::get)
        .collect();
    let event_server_control_ep = config.event_server_control_ep;
    let benchmark_server_ep = config.benchmark_server_ep.get();

    let irq_map = IRQMap {
        ppi: config
            .ppi_map
            .into_iter()
            .map(|(ppi, (in_index, must_ack))| (ppi, (in_index.to_nat(), must_ack)))
            .collect(),
        spi: config
            .spi_map
            .into_iter()
            .map(|(spi, (in_index, nid, must_ack))| (spi, (in_index.to_nat(), nid, must_ack)))
            .collect(),
    };

    VMMConfig {
        debug: false,
        cnode: config.cnode.get(),
        gic_lock: config.gic_lock.get(),
        nodes_lock: config.nodes_lock.get(),
        event_server_client_ep,
        irq_map,
        gic_dist_paddr: config.gic_dist_paddr,
        nodes: config
            .nodes
            .iter()
            .enumerate()
            .map(|(i, node)| VMMNodeConfig {
                tcb: node.tcb.get(),
                vcpu: node.vcpu.get(),
                ep: node.ep_read.get(),
                fault_reply_slot: node.fault_reply_slot.get(),
                thread: node.thread.get(),
                event_server_bitfield: node.event_server_bitfield,
                extension: Extension {
                    resource_server_ep: resource_server_ep[i].get(),
                    event_server_control_ep: event_server_control_ep[i].get(),
                    benchmark_server_ep,
                },
            })
            .collect(),
    }
    .run()
}

struct Extension {
    resource_server_ep: Endpoint,
    #[allow(dead_code)]
    event_server_control_ep: Endpoint,
    #[allow(dead_code)]
    benchmark_server_ep: Endpoint,
}

impl VMMExtension for Extension {
    fn handle_wf(_node: &mut VMMNode<Self>) -> Fallible<()> {
        panic!()
    }

    fn handle_syscall(node: &mut VMMNode<Self>, fault: &UnknownSyscall) -> Fallible<()> {
        Ok(match fault.syscall() {
            sys_id::RESOURCE_SERVER_PASSTHRU => Self::sys_resource_server_passthru(node, fault)?,
            sys_id::DIRECT => Self::sys_direct(node, fault)?,
            _ => {
                panic!("unknown syscall: {:?}", fault)
            }
        })
    }

    fn handle_putchar(_node: &mut VMMNode<Self>, c: u8) -> Fallible<()> {
        debug_print!("{}", c as char);
        Ok(())
    }
}

impl Extension {
    fn userspace_syscall(
        node: &mut VMMNode<Self>,
        fault: &UnknownSyscall,
        f: impl FnOnce(&mut VMMNode<Self>, &[u8]) -> Fallible<Vec<u8>>,
    ) -> Fallible<()> {
        let in_len_words = fault.gpr(0) as usize;
        let mut in_vec = vec![];
        for i in 0..in_len_words {
            in_vec.extend(fault.gpr(i + 1).to_ne_bytes());
        }
        let mut out_vec = f(node, &in_vec)?;
        let out_len_words = round_up(out_vec.len(), mem::size_of::<u64>()) / mem::size_of::<u64>();
        assert!(out_len_words <= 6);
        sel4::with_borrow_ipc_buffer_mut(|ipcbuf| {
            *UnknownSyscall::mr_gpr(ipcbuf, 0) = out_len_words as u64;
        });
        out_vec.resize_with(out_len_words * mem::size_of::<u64>(), || 0);
        for i in 0..out_len_words {
            sel4::with_borrow_ipc_buffer_mut(|ipcbuf| {
                *UnknownSyscall::mr_gpr(ipcbuf, i + 1) = u64::from_ne_bytes(
                    <[u8; mem::size_of::<u64>()]>::try_from(
                        &out_vec[mem::size_of::<u64>() * i..][..mem::size_of::<u64>()],
                    )
                    .unwrap(),
                )
            });
        }
        fault.advance_and_reply();
        Ok(())
    }

    fn sys_resource_server_passthru(
        node: &mut VMMNode<Self>,
        fault: &UnknownSyscall,
    ) -> Fallible<()> {
        Self::userspace_syscall(node, fault, |node, bytes| {
            let send_info = rpc::prepare_bytes_for_send(bytes)?;
            let recv_info = node.extension.resource_server_ep.call(send_info);
            let resp = sel4::with_borrow_ipc_buffer(|ipc_buffer| {
                let n = (sel4::WORD_SIZE / mem::size_of::<sel4::Word>()) * recv_info.length();
                ipc_buffer.msg_bytes()[..n].to_vec()
            });
            Ok(resp)
        })
    }

    fn sys_direct(node: &mut VMMNode<Self>, fault: &UnknownSyscall) -> Fallible<()> {
        Self::userspace_syscall(node, fault, |node, bytes| {
            let request = postcard::from_bytes(bytes).unwrap();
            let response = Self::direct(node, &request)?;
            Ok(postcard::to_allocvec(&response).unwrap())
        })
    }

    sel4_cfg_if! {
        if #[cfg(not(KERNEL_BENCHMARK = "none"))] {
            fn direct(node: &mut VMMNode<Self>, request: &DirectRequest)
                      -> Fallible<DirectResponse>
            {
                match request {
                    DirectRequest::BenchmarkUtilisationStart => {
                        rpc::Client::new(node.extension.benchmark_server_ep)
                            .call::<benchmark_server::Response>(
                                &benchmark_server::Request::Start
                            )
                            .unwrap()
                            .unwrap();
                    }
                    DirectRequest::BenchmarkUtilisationFinish => {
                        rpc::Client::new(node.extension.benchmark_server_ep)
                            .call::<benchmark_server::Response>(
                                &benchmark_server::Request::Finish
                            )
                            .unwrap()
                            .unwrap();
                    }
                }
                Ok(DirectResponse)
            }
        } else {
            fn direct(_node: &mut VMMNode<Self>, _request: &DirectRequest)
                      -> Fallible<DirectResponse>
            {
                panic!()
            }
        }
    }
}

const fn round_up(n: usize, b: usize) -> usize {
    let r = n % b;
    n + if r == 0 { 0 } else { b - r }
}
