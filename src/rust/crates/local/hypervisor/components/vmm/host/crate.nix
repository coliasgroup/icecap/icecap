{ mk, localCrates, postcardWith, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-host-vmm";
  nix.local.dependencies = with localCrates; [
    biterate
    finite-set
    hypervisor-host-vmm-config
    sel4-supervising
    icecap-core
    sel4-simple-task-runtime
    hypervisor-vmm-core
    hypervisor-event-server-types
    hypervisor-resource-server-types
    hypervisor-benchmark-server-types
    hypervisor-host-vmm-types
  ];
  dependencies = {
    inherit (versions) cfg-if ;
    postcard = postcardWith [ "alloc" ];
  };
}
