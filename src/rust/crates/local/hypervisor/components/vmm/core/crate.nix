{ mk, localCrates, versions }:

mk {
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-vmm-core";
  nix.local.dependencies = with localCrates; [
    biterate
    sel4-supervising
    icecap-core
    icecap-vmm-gic
    icecap-vmm-psci
    icecap-vmm-utils
    hypervisor-event-server-types
  ];
  dependencies = {
    icecap-core.features = [ "full" ];
    inherit (versions) log ;
  };
}
