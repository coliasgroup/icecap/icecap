{ mk, localCrates }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-realm-vmm";
  nix.local.dependencies = with localCrates; [
    biterate
    finite-set
    hypervisor-realm-vmm-config
    icecap-core
    sel4-simple-task-runtime
    hypervisor-vmm-core
    hypervisor-event-server-types
  ];
  dependencies = {
  };
}
