#![no_std]
#![no_main]

extern crate alloc;

use finite_set::Finite;
use hypervisor_realm_vmm_config::*;
use hypervisor_vmm_core::*;
use icecap_core::prelude::*;
use icecap_core::sel4::UnknownSyscall;

#[sel4_simple_task_runtime::main_postcard]
pub fn main(config: Config) -> Fallible<()> {
    let event_server_client_ep = config
        .event_server_client_ep
        .iter()
        .map(ConfigCPtr::get)
        .collect();

    let irq_map = IRQMap {
        ppi: config
            .ppi_map
            .into_iter()
            .map(|(ppi, (in_index, must_ack))| (ppi, (in_index.to_nat(), must_ack)))
            .collect(),
        spi: config
            .spi_map
            .into_iter()
            .map(|(spi, (in_index, nid, must_ack))| (spi, (in_index.to_nat(), nid, must_ack)))
            .collect(),
    };

    VMMConfig {
        debug: true,
        cnode: config.cnode.get(),
        gic_lock: config.gic_lock.get(),
        nodes_lock: config.nodes_lock.get(),
        event_server_client_ep,
        irq_map,
        gic_dist_paddr: config.gic_dist_paddr,
        nodes: config
            .nodes
            .iter()
            .map(|node| VMMNodeConfig {
                tcb: node.tcb.get(),
                vcpu: node.vcpu.get(),
                ep: node.ep_read.get(),
                fault_reply_slot: node.fault_reply_slot.get(),
                event_server_bitfield: node.event_server_bitfield,
                thread: node.thread.get(),
                extension: Extension {},
            })
            .collect(),
    }
    .run()
}

struct Extension {}

impl VMMExtension for Extension {
    fn handle_wf(_node: &mut VMMNode<Self>) -> Fallible<()> {
        panic!()
    }

    fn handle_syscall(_node: &mut VMMNode<Self>, fault: &UnknownSyscall) -> Fallible<()> {
        #[allow(unreachable_code)]
        Ok(match fault.syscall() {
            _ => {
                panic!("unknown syscall")
            }
        })
    }

    fn handle_putchar(_node: &mut VMMNode<Self>, c: u8) -> Fallible<()> {
        debug_print!("{}", c as char);
        Ok(())
    }
}
