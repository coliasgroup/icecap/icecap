{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-realm-vmm-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    hypervisor-event-server-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
