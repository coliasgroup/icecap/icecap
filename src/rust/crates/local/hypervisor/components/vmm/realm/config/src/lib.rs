#![no_std]

extern crate alloc;

use alloc::collections::BTreeMap;
use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use hypervisor_event_server_types::events::{RealmIn, RealmOut};
use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub cnode: ConfigCPtr<CNode>,
    pub gic_lock: ConfigCPtr<Notification>,
    pub nodes_lock: ConfigCPtr<Notification>,
    pub gic_dist_paddr: usize,
    pub nodes: Vec<Node>,
    pub event_server_client_ep: Vec<ConfigCPtr<Endpoint>>,

    pub ppi_map: BTreeMap<usize, (RealmIn, bool)>,
    pub spi_map: BTreeMap<usize, (RealmIn, usize, bool)>, // in_index, nid
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Node {
    pub tcb: ConfigCPtr<TCB>,
    pub vcpu: ConfigCPtr<VCPU>,
    pub thread: ConfigCPtr<StaticThread>,
    pub ep_read: ConfigCPtr<Endpoint>,
    pub fault_reply_slot: ConfigCPtr<Endpoint>,
    pub event_server_bitfield: usize,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum KickConfig {
    Notification(ConfigCPtr<Notification>),
    OutIndex(RealmOut),
}
