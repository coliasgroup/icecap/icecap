{ mk, localCrates, serdeWith, stdenv, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-mirage";
  nix.local.dependencies = with localCrates; [
    finite-set
    icecap-linux-syscall-types
    icecap-linux-syscall-musl
    icecap-core
    sel4-simple-task-runtime
    icecap-ring-buffer
    icecap-ring-buffer-config
    hypervisor-mirage-config
    hypervisor-event-server-types
    icecap-mirage-core
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
    cortex-a = "8.1.1";
    tock-registers = { version = versions.tock-registers; default-features = false; };
  };
  build-dependencies = {
    cc = "1.0.76";
    glob = "0.3.0";
  };
  # nix.buildScript = {
  #   NOTE this doesn't work because of circular dependencies. rustc deduplicates these.
  #   rustc-link-lib = [
  #     "hypervisor-mirage-glue" "hypervisor-mirage" "sel4asmrun" "c" "gcc"
  #   ];
  # };
  nix.meta.skip = true;
}
