#![no_std]

extern crate alloc;

use alloc::vec::Vec;

use serde::{Deserialize, Serialize};

use icecap_ring_buffer_config::*;
use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub lock: ConfigCPtr<Notification>,

    pub event: ConfigCPtr<Notification>,
    pub event_server_endpoint: ConfigCPtr<Endpoint>,
    pub event_server_bitfield: usize,

    pub net_rb: RingBufferConfig,

    pub passthru: Vec<u8>,
}
