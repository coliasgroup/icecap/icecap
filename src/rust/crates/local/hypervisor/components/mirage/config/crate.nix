{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-mirage-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
    icecap-ring-buffer-config
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
