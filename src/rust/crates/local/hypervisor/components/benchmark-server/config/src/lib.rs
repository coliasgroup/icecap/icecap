#![no_std]

extern crate alloc;

use serde::{Deserialize, Serialize};

use sel4_simple_task_config_types::*;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub self_tcb: ConfigCPtr<TCB>,
    pub ep: ConfigCPtr<Endpoint>,
}
