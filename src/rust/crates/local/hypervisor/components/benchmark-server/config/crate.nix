{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-benchmark-server-config";
  nix.local.dependencies = with localCrates; [
    sel4-simple-task-config-types
  ];
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
