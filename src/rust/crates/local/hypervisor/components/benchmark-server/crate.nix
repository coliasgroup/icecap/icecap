{ mk, localCrates, versions }:

mk {
  nix.meta.labels = [ "leaf" ];
  nix.meta.requirements = [ "sel4" ];
  package.name = "hypervisor-benchmark-server";
  nix.local.dependencies = with localCrates; [
    icecap-core
    sel4-simple-task-runtime
    hypervisor-benchmark-server-types
    hypervisor-benchmark-server-config
  ];
  dependencies = {
    inherit (versions) cfg-if ;
  };
}
