#![no_std]
#![no_main]
#![feature(extract_if)]
#![feature(never_type)]

use hypervisor_benchmark_server_config::*;
use hypervisor_benchmark_server_types::*;
use icecap_core::prelude::*;
use icecap_core::rpc::easy as rpc;

#[sel4_simple_task_runtime::main_postcard]
pub fn main(config: Config) -> Fallible<()> {
    let ep = config.ep.get();
    let tcb = config.self_tcb.get();
    loop {
        let request = rpc::server::recv(ep, |reception| reception.read())?;
        let response = handle(tcb, &request)?;
        rpc::server::reply(&response)?;
    }
}

sel4_cfg_if! {
    if #[cfg(not(KERNEL_BENCHMARK = "none"))] {
        const NUM_CORES: usize = sel4::sel4_cfg_usize!(MAX_NUM_NODES);

        fn handle(tcb: TCB, request: &Request) -> Fallible<Response> {
            match request {
                Request::Start => {
                    debug_println!("benchmark-server: start");
                    for affinity in 0..NUM_CORES {
                        tcb.tcb_set_affinity(affinity as u64)?;
                        sel4::benchmark_reset_log()?;
                        sel4::benchmark_reset_all_thread_utilisation();
                    }
                }
                Request::Finish => {
                    for affinity in 0..NUM_CORES {
                        tcb.tcb_set_affinity(affinity as u64)?;
                        assert_eq!(sel4::benchmark_finalize_log(), 0);
                        sel4::benchmark_dump_all_thread_utilisation();
                    }
                }
            }
            Ok(Ok(InnerResponse))
        }
    } else {
        fn handle(_tcb: TCB, _request: &Request) -> Fallible<Response> {
            Ok(Err(Error))
        }
    }
}
