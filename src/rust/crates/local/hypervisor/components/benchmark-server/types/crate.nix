{ mk, localCrates, serdeWith }:

mk {
  package.name = "hypervisor-benchmark-server-types";
  dependencies = {
    serde = serdeWith [ "alloc" "derive" ];
  };
}
