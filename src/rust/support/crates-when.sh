set -eu

export LC_COLLATE=C

here=$(dirname $0)
crate_sets_dir=$here/crate-sets

all_name="$1"
shift

all="$crate_sets_dir/$all_name.txt"

grep_args=
for have in $@; do
    grep_args="$grep_args -e $have"
done

dont_haves=$(ls $crate_sets_dir | sed -nr 's,require-(.*).txt,\1,p' | grep -v $grep_args)

pipe="cat $all"
for dont_have in $dont_haves; do
    pipe="$pipe | comm - $crate_sets_dir/require-$dont_have.txt -23"
done

eval "$pipe"
