{ lib }:

lib.mapAttrs (_: v: "crates/local/${v}") {

  # icecap

  icecap-core = "framework/base/icecap-core";
  icecap-component-config-cli-core = "framework/base/icecap-component-config/cli/core";
  icecap-failure = "framework/base/icecap-failure";
  icecap-failure-derive = "framework/base/icecap-failure/derive";

  # helpers

  absurdity = "framework/helpers/absurdity";
  biterate = "framework/helpers/biterate";
  finite-set = "framework/helpers/finite-set";
  finite-set-derive = "framework/helpers/finite-set/derive";
  generated-module-hack = "framework/helpers/generated-module-hack";
  more-env-macros = "framework/helpers/more-env-macros";

  cfg-if-generic = "framework/helpers/cfg-if-generic";

  icecap-fdt = "framework/helpers/icecap-fdt";

  # 9p

  crosvm-9p = "framework/9p/crosvm-9p";
  crosvm-9p-wire-format-derive = "framework/9p/crosvm-9p/wire-format-derive";
  crosvm-9p-server = "framework/9p/crosvm-9p-server";
  crosvm-9p-server-cli = "framework/9p/crosvm-9p-server/cli";

  # icecap extra

  sel4-supervising = "framework/extra/sel4-supervising";

  icecap-driver-interfaces = "framework/extra/drivers/icecap-driver-interfaces";
  icecap-bcm-system-timer-driver = "framework/extra/drivers/devices/bcm-system-timer";
  icecap-bcm2835-aux-uart-driver = "framework/extra/drivers/devices/bcm2835-aux-uart";
  icecap-pl011-driver = "framework/extra/drivers/devices/pl011";
  icecap-virt-timer-driver = "framework/extra/drivers/devices/virt-timer";

  icecap-vmm-gic = "framework/extra/vmm/gic";
  icecap-vmm-psci = "framework/extra/vmm/psci";
  icecap-vmm-utils = "framework/extra/vmm/utils";
  icecap-vmm-fdt-bindings = "framework/extra/vmm/fdt-bindings";

  icecap-ring-buffer = "framework/extra/ring-buffer";
  icecap-ring-buffer-config = "framework/extra/ring-buffer/config";

  icecap-linux-syscall-types = "framework/extra/linux-syscall/types";
  icecap-linux-syscall-musl = "framework/extra/linux-syscall/musl";

  icecap-mirage-core = "framework/extra/mirage/core";

  icecap-capdl-dynamic-realize = "framework/extra/capdl-dynamic/realize";
  icecap-capdl-dynamic-realize-simple = "framework/extra/capdl-dynamic/realize/simple";
  icecap-capdl-dynamic-realize-simple-config = "framework/extra/capdl-dynamic/realize/simple/config";
  icecap-capdl-dynamic-types = "framework/extra/capdl-dynamic/types";
  icecap-serialize-dynamic-capdl-spec = "framework/extra/capdl-dynamic/cli/serialize-dynamic-capdl-spec";

  icecap-generic-timer-server = "framework/extra/generic-components/timer-server";
  icecap-generic-timer-server-types = "framework/extra/generic-components/timer-server/types";
  icecap-generic-timer-server-client = "framework/extra/generic-components/timer-server/client";
  icecap-generic-timer-server-config = "framework/extra/generic-components/timer-server/config";
  icecap-generic-serial-server-core = "framework/extra/generic-components/serial-server/core";
  icecap-serialize-generic-component-config = "framework/extra/generic-components/icecap-serialize-generic-component-config";

  icecap-std-external = "framework/extra/std-support/icecap-std-external";
  icecap-std-impl = "framework/extra/std-support/icecap-std-impl";

  # tests

  tests-capdl-sysroot-test = "framework/tests/capdl/sysroot/test";

  # examples

  examples-minimal = "framework/examples/minimal";
  examples-minimal-capdl-component-example = "framework/examples/minimal-capdl/components/example";
  examples-minimal-capdl-component-example-config = "framework/examples/minimal-capdl/components/example/config";
  examples-minimal-capdl-component-example-config-cli = "framework/examples/minimal-capdl/components/example/config/cli";
  examples-basic-system-component-application = "framework/examples/basic-system/components/application";
  examples-basic-system-component-timer-server = "framework/examples/basic-system/components/timer-server";
  examples-basic-system-component-timer-server-types = "framework/examples/basic-system/components/timer-server/types";
  examples-basic-system-component-serial-server = "framework/examples/basic-system/components/serial-server";
  examples-dynamism-component-subcomponent = "framework/examples/dynamism/components/subcomponent";
  examples-dynamism-component-supercomponent = "framework/examples/dynamism/components/supercomponent";


  ### HYPERVISOR ###

  hypervisor-resource-server = "hypervisor/components/resource-server";
  hypervisor-resource-server-types = "hypervisor/components/resource-server/types";
  hypervisor-resource-server-core = "hypervisor/components/resource-server/core";
  hypervisor-resource-server-config = "hypervisor/components/resource-server/config";

  hypervisor-event-server = "hypervisor/components/event-server";
  hypervisor-event-server-types = "hypervisor/components/event-server/types";
  hypervisor-event-server-config = "hypervisor/components/event-server/config";

  hypervisor-serial-server = "hypervisor/components/serial-server";
  hypervisor-serial-server-config = "hypervisor/components/serial-server/config";

  hypervisor-benchmark-server = "hypervisor/components/benchmark-server";
  hypervisor-benchmark-server-types = "hypervisor/components/benchmark-server/types";
  hypervisor-benchmark-server-config = "hypervisor/components/benchmark-server/config";

  hypervisor-fault-handler = "hypervisor/components/fault-handler";
  hypervisor-fault-handler-config = "hypervisor/components/fault-handler/config";

  hypervisor-idle = "hypervisor/components/idle";

  hypervisor-vmm-core = "hypervisor/components/vmm/core";
  hypervisor-host-vmm = "hypervisor/components/vmm/host";
  hypervisor-host-vmm-types = "hypervisor/components/vmm/host/types";
  hypervisor-host-vmm-config = "hypervisor/components/vmm/host/config";
  hypervisor-realm-vmm = "hypervisor/components/vmm/realm";
  hypervisor-realm-vmm-config = "hypervisor/components/vmm/realm/config";

  hypervisor-mirage = "hypervisor/components/mirage";
  hypervisor-mirage-config = "hypervisor/components/mirage/config";

  icecap-host = "hypervisor/host-tools/icecap-host";
  icecap-host-core = "hypervisor/host-tools/icecap-host/core";

  hypervisor-fdt-append-devices = "hypervisor/build-tools/hypervisor-fdt-append-devices";
  hypervisor-serialize-component-config = "hypervisor/build-tools/hypervisor-serialize-component-config";
  hypervisor-serialize-event-server-out-index = "hypervisor/build-tools/hypervisor-serialize-event-server-out-index";

  hypervisor-tests-capdl-benchmark-server-test = "hypervisor/tests/capdl/benchmark-server/test";

  hypervisor-examples-realms-minimal = "hypervisor/examples/realms/minimal";
}
