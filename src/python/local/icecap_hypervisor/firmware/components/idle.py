from icecap_framework import IceCapElfComponent

class Idle(IceCapElfComponent):

    def arg(self):
        return self.empty_arg()
