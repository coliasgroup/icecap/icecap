import json
import os
import shutil
import subprocess
import yaml
from collections import namedtuple
from pathlib import Path

from capdl import ObjectType, ObjectAllocator, CSpaceAllocator, AddressSpaceAllocator, Untyped, lookup_architecture, register_object_sizes
from capdl.Allocator import RenderState, Cap, ASIDTableAllocator, BestFitAllocator, RenderState

from capdl_simple_composition import BaseComposition
from icecap_framework.utils import aligned_chunks, as_, as_list, BLOCK_SIZE, BLOCK_SIZE_BITS, PAGE_SIZE, PAGE_SIZE_BITS

ARCH = 'aarch64'

RingBufferObjects = namedtuple('RingBufferObjects', 'read write')
RingBufferSideObjects = namedtuple('RingBufferSideObjects', 'size ctrl data')

class IceCapBaseComposition(BaseComposition):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._gic_vcpu_frame = None # allocate lazily

    def alloc_region(self, tag, region_size):
        if region_size & ((1 << 21) - 1) == 0:
            frame_size_bits = 21
        else:
            assert region_size & ((1 << 12) - 1) == 0
            frame_size_bits = 12
        frame_size = 1 << frame_size_bits
        num_frames = region_size >> frame_size_bits
        for i in range(num_frames):
            yield self.alloc(ObjectType.seL4_FrameObject, '{}_{}'.format(tag, i), size=frame_size), frame_size_bits

    def alloc_ring_buffer_raw(self, a_name, a_size_bits, b_name, b_size_bits):

        ctrl_size_bits = 12

        def mk(x_name, y_name, x_size_bits):
            return RingBufferSideObjects(
                size=1 << x_size_bits,
                ctrl=tuple(self.alloc_region('ring_buffer_ctrl_{}_to_{}'.format(x_name, y_name), 1 << ctrl_size_bits)),
                data=tuple(self.alloc_region('ring_buffer_data_{}_to_{}'.format(x_name, y_name), 1 << x_size_bits)),
            )

        a_to_b = mk(a_name, b_name, a_size_bits)
        b_to_a = mk(b_name, a_name, b_size_bits)

        return a_to_b, b_to_a

    def alloc_ring_buffer(self, a_name, a_size_bits, b_name, b_size_bits):
        a_to_b, b_to_a = self.alloc_ring_buffer_raw(a_name, a_size_bits, b_name, b_size_bits)
        a_objs = RingBufferObjects(write=a_to_b, read=b_to_a)
        b_objs = RingBufferObjects(write=b_to_a, read=a_to_b)
        return a_objs, b_objs

    def create_gic_vcpu_frame(self):
        raise NotImplementedError

    def gic_vcpu_frame(self):
        if self._gic_vcpu_frame is None:
            self._gic_vcpu_frame = self.create_gic_vcpu_frame()
        return self._gic_vcpu_frame

    # TODO move
    def serialize_event_server_out(self, role, index):
        index = json.dumps(index)
        p = subprocess.run(["hypervisor-serialize-event-server-out-index", role, index], check=True, stdout=subprocess.PIPE)
        return int(p.stdout)
