import json
import subprocess
from pathlib import Path
from elftools.elf.elffile import ELFFile

from capdl import ObjectType, ELF, Cap

from icecap_framework.utils import BLOCK_SIZE
from capdl_simple_composition import ElfComponent

DEFAULT_STATIC_HEAP_SIZE = 4 * BLOCK_SIZE

DEFAULT_AFFINITY = 0
DEFAULT_PRIO = 128
DEFAULT_MAX_PRIO = 0
DEFAULT_STACK_SIZE = BLOCK_SIZE

class IceCapElfComponent(ElfComponent):

    def __init__(self, *args, fault_handler=None, **kwargs):
        self.fault_handler = fault_handler
        super().__init__(*args, **kwargs)

    def register_thread(self, thread):
        if self.fault_handler is not None:
            self.fault_handler.handle(thread)

    def serialize_generic_component_arg(self, ty):
        return ['icecap-serialize-generic-component-config', ty]

    def map_ring_buffer(self, objs, cached=True):
        return {
            'read': {
                'size': objs.read.size,
                'ctrl': self.map_region(objs.read.ctrl, read=True, write=True, cached=cached),
                'data': self.map_region(objs.read.data, read=True, cached=cached),
                },
            'write': {
                'size': objs.write.size,
                'ctrl': self.map_region(objs.write.ctrl, read=True, write=True, cached=cached),
                'data': self.map_region(objs.write.data, read=True, write=True, cached=cached),
                },
            }

    def map_region(self, region, **perms):
        self.pad_and_align_to_page()
        start = self.align(1 << region[0][1]) # HACK
        vaddr = start
        for frame, size_bits in region:
            assert vaddr & ((1 << size_bits) - 1) == 0
            cap = Cap(frame, **perms)
            self.addr_space().add_hack_page(vaddr, 1 << size_bits, cap)
            vaddr += 1 << size_bits
        self.set_cursor(vaddr)
        return start
