from capdl_simple_composition.utils import *

PAGE_SIZE_BITS = 12
PAGE_SIZE = 1 << PAGE_SIZE_BITS

BLOCK_SIZE_BITS = 21
BLOCK_SIZE = 1 << BLOCK_SIZE_BITS
