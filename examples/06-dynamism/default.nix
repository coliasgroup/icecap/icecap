{ framework ? import ../../nix/framework }:

let
  inherit (framework) lib pkgs;

  configured = pkgs.none.icecap.configured.virt;

  inherit (pkgs.none.icecap) icecapSrc platUtils globalCrates;
  inherit (configured) mkIceDL serializeDynDLSpec;

in rec {

  run = platUtils.${configured.icecapPlat}.bundle {
    inherit (composition) image;
  };

  composition = configured.compose {
    script = icecapSrc.absolute ./supersystem.py;
    config = {
      components = {
        supercomponent.image = components.supercomponent.elf;
      };
      subsystem_spec = serializedSubsystem;
    };
  };

  subsystem = mkIceDL {
    script = icecapSrc.absolute ./subsystem.py;
    config = {
      num_cores = 1;
      components = {
        subcomponent.image = components.subcomponent.elf;
      };
    };
  };

  serializedSubsystem = serializeDynDLSpec {
    spec = "${subsystem}/spec.cdl";
    root = "${subsystem}/links";
  };

  components = lib.mapAttrs (crateName: _: configured.buildIceCapComponent {
    rootCrate = globalCrates."examples-dynamism-component-${crateName}";
  }) {
    subcomponent = null;
    supercomponent = null;
  };

}
