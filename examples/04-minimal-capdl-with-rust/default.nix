{ framework ? import ../../nix/framework }:

let
  inherit (framework) lib pkgs;

  configured = pkgs.none.icecap.configured.virt;

  inherit (pkgs.dev.icecap) buildIceCapRustPackageIncrementally;
  inherit (pkgs.none.icecap) icecapSrc platUtils globalCrates;

in rec {

  run = platUtils.${configured.icecapPlat}.bundle {
    inherit (composition) image;
  };

  composition = configured.compose {
    script = icecapSrc.absolute ./cdl.py;
    config = {
      components = {
        example_component.image = example-component.elf;
      };
      tools.serialize-example-component-config = "${serialize-example-component-config}/bin/examples-minimal-capdl-component-example-config-cli";
    };
  };

  example-component = configured.buildIceCapComponent {
    rootCrate = globalCrates.examples-minimal-capdl-component-example;
  };

  serialize-example-component-config = buildIceCapRustPackageIncrementally {
    rootCrate = globalCrates.examples-minimal-capdl-component-example-config-cli;
  };

}
