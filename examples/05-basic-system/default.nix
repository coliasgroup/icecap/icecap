{ framework ? import ../../nix/framework }:

let
  inherit (framework) lib pkgs;

  configured = pkgs.none.icecap.configured.virt;

  inherit (pkgs.dev.icecap) buildIceCapRustPackageIncrementally;
  inherit (pkgs.none.icecap) icecapSrc platUtils globalCrates;

in rec {

  run = platUtils.${configured.icecapPlat}.bundle {
    inherit (composition) image;
  };

  composition = configured.compose {
    script = "${icecapSrc.absolute ./cdl}/composition.py";
    config = {
      components = with components; {
        application.image = application.elf;
        serial_server.image = serial-server.elf;
        timer_server.image = timer-server.elf;
      };
    };
  };

  components = lib.mapAttrs (crateName: _: configured.buildIceCapComponent {
    rootCrate = globalCrates."examples-basic-system-component-${crateName}";
  }) {
    application = null;
    serial-server = null;
    timer-server = null;
  };

}
