{ lib, pkgs }:

let
  configured = pkgs.none.icecap.configured.virt;

  inherit (pkgs.dev) writeText;
  inherit (pkgs.none.icecap) globalCrates;
  inherit (configured) mkRealm;

in rec {

  spec = mkRealm {
    script = ./ddl.py;
    config = {
      realm_id = 0;
      num_cores = 1;
      components = {
        minimal.image = minimal.elf;
      };
    };
  };

  minimal = configured.buildIceCapComponent {
    rootCrate = globalCrates.hypervisor-examples-realms-minimal;
    release = false;
  };

}
