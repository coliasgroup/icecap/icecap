{ framework ? import ../../nix/framework }:

let
  inherit (framework) lib pkgs;

  configured = pkgs.none.icecap.configured.virt;

  inherit (pkgs.none.icecap) icecapSrc platUtils globalCrates;

in rec {

  run = platUtils.${configured.icecapPlat}.bundle {
    inherit (composition) image;
  };

  composition = configured.compose {
    appELF = root-task.split;
  };

  root-task = configured.buildIceCapComponent {
    rootCrate = globalCrates.examples-minimal;
  };

}
