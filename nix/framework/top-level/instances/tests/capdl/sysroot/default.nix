{ mkInstance
, icecapSrc
, globalCrates
}:

mkInstance {} (self: with self.configured; with self; {

  broken = true;

  composition = compose {
    script = icecapSrc.absolute ./cdl.py;
    config = {
      components = {
        test.image = test.split;
      };
    };
  };

  test = buildIceCapComponent {
    rootCrate = globalCrates.tests-capdl-sysroot-test;
    replaceSysroot = configured.rustSysrootWithStd;
    extraProfile = {
      panic = "abort";
    };
  };

})
