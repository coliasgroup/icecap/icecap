{ lib, pkgs }:

{ configured }:

let
  inherit (configured) icecapPlat;
in

f: lib.fix (self:

  let
    attrs = f self;
  in

  with self; {

    inherit configured;

    composition = null;
    hostPayload = {};
    extraLinks = {};
    icecapPlatArgs = {
      virt = {
        devScript = true;
      };
    };

    run = pkgs.none.icecap.platUtils.${icecapPlat}.bundle {
      inherit (composition) image;
      inherit hostPayload;
      platArgs = icecapPlatArgs.${icecapPlat} or {};
      extraLinks = {
        composition = composition.display;
        "debug/show-backtrace" = "${pkgs.dev.icecap.sel4-backtrace-cli}/bin/sel4-symbolize-backtrace";
        # HACK
        "debug/root-show-backtrace" = pkgs.dev.writeScript "x.sh" ''
          #!${pkgs.dev.runtimeShell}
          exec ${pkgs.dev.icecap.sel4-backtrace-cli}/bin/sel4-symbolize-backtrace -f ./result/composition/breakdown/app.debug.elf "$@"
        '';
      } // extraLinks;
    };

  } // attrs
)
