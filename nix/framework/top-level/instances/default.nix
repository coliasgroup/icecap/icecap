{ lib, pkgs }:

let

  mkInstanceWith = import ./mk-instance.nix {
    inherit lib pkgs;
  };

  callInstance = path: args: lib.flip lib.mapAttrs pkgs.none.icecap.configured (_: configured:
    pkgs.none.icecap.newScope {
      inherit configured;
      mkInstance = icecapConfigOverride: mkInstanceWith {
        configured = configured.override' icecapConfigOverride;
      };
    } path args
  );

in {
  tests = {
    capdl = {
      backtrace = callInstance ./tests/capdl/backtrace {};
      # sysroot = callInstance ./tests/capdl/sysroot {};
    };
  };

  inherit mkInstanceWith callInstance;
}
