{ lib, pkgs, mkEverything, instances, automatedTests, generatedSources }:

let
  inherit (pkgs) dev none linux musl;

  forEachIn = lib.flip lib.concatMap;
  forEachConfigured = f: lib.mapAttrsToList (lib.const f) pkgs.none.icecap.configured;

in mkEverything {

  cached = [
    (map (lib.mapAttrsToList (_: plat: plat.run)) [
      # good
      # instances.tests.capdl.sysroot
    ])

    automatedTests.runAll

    generatedSources.update
  ];

  extraPure = [
    (forEachIn [ dev linux musl ] (host: [
      host.icecap.crosvm-9p-server
    ]))
  ];
}
