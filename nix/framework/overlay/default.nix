self: super: with self;

{

  icecap =
    let
      otherSplices = generateSplicesForMkScope "icecap";
    in
      lib.makeScopeWithSplicing
        splicePackages
        newScope
        otherSplices
        (_: {})
        (_: {})
        (self: callPackage ../scope {} self // {
          __dontMashWhenSplicingChildren = true;
          __dontSpliceChildren = true; # for performance
          inherit otherSplices; # for convenience
        })
      ;

  stdenv =
    if super.stdenv.hostPlatform.isNone
    # Use toolchain without newlib. This is equivalent to crossLibcStdenv.
    then super.overrideCC super.stdenv super.crossLibcStdenv.cc
    else super.stdenv;

  # Add Python packages needed by the seL4 ecosystem
  pythonPackagesExtensions = super.pythonPackagesExtensions ++ [
    (callPackage ./python-overrides.nix {})
  ];

}
