{ lib, stdenv, buildPlatform, hostPlatform, buildPackages, pkgsBuildBuild, linkFarm }:

self: with self;

let

  rustToolchainParams = {
    channel = "nightly";
    date = "2023-08-02";
    sha256 = "sha256-LMK50izxGqjJVPhVG+C2VosDHvDYNXhaOFSb8fwGf/Y=";
  };

  rustToolchainTargets = {
    inherit devPkgs linuxPkgs muslPkgs;
  };

  rustToolchains = lib.flip lib.mapAttrs rustToolchainTargets (_k: v:
    fenix.targets.${v.icecap.defaultRustTargetName}.toolchainOf rustToolchainParams
  );

  rustToolchain = fenix.combine [
    rustToolchains.devPkgs.completeToolchain
    rustToolchains.linuxPkgs.rust-std
    rustToolchains.muslPkgs.rust-std
  ];

  fenixRev = "260b00254fc152885283b0d2aec78547a1f77efd";
  fenixSource = fetchTarball "https://github.com/nix-community/fenix/archive/${fenixRev}.tar.gz";
  fenix = import fenixSource {};

in

{

  defaultRustToolchain = rustToolchain;

  icecapRustTargetName = arch: "${arch}-sel4";

  rustTargetArchName = {
    aarch64 = "aarch64";
    riscv64 = "riscv64imac";
    x86_64 = "x86_64";
  }."${hostPlatform.parsed.cpu.name}";

  defaultRustTargetInfo =
    if !hostPlatform.isNone
    then mkBuiltinRustTargetInfo hostPlatform.config
    else seL4RustTargetInfoWithConfig {};

  seL4RustTargetInfoWithConfig = { cp ? false, minimal ? false }: mkCustomRustTargetInfo "${rustTargetArchName}-sel4${lib.optionalString cp "cp"}${lib.optionalString minimal "-minimal"}";

  bareMetalRustTargetInfo = mkBuiltinRustTargetInfo {
    aarch64 = "aarch64-unknown-none";
    riscv64 = "riscv64imac-unknown-none-elf";
    x86_64 = "x86_64-unknown-none";
  }."${hostPlatform.parsed.cpu.name}";

  mkBuiltinRustTargetInfo = name: {
    inherit name;
    path = null;
  };

  mkCustomRustTargetInfo = name: {
    inherit name;
    path =
      let
        fname = "${name}.json";
      in
        linkFarm "targets" [
          { name = fname; path = icecapSrc.relative "external/rust-seL4/support/targets/${fname}"; }
        ];
  };

  defaultRustTargetName = defaultRustTargetInfo.name;
  defaultRustTargetPath = defaultRustTargetInfo.path;

  chooseLinkerForRustTarget = { rustToolchain, rustTargetName, platform }:
    let
      env = if platform.config == buildPlatform.config then buildPackages.stdenv else stdenv;
    in
      if platform.isNone
      then "${rustToolchain}/lib/rustlib/${buildPlatform.config}/bin/rust-lld"
      else (
        if platform.isMusl /* HACK for proper static linking on musl */
        then "${env.cc.targetPrefix}ld"
        else "${env.cc.targetPrefix}cc"
      )
  ;

  ###

  topLevelLockfile = icecapSrc.relativeRaw "rust/Cargo.lock";

  buildIceCapRustPackageIncrementally = buildCrateInLayers {
    superLockfile = topLevelLockfile;
  };

  toAnonymousTOMLFile = toTOMLFile "x.toml";

  ###

  libclangPath = "${lib.getLib pkgsBuildBuild.llvmPackages.libclang}/lib";

}
