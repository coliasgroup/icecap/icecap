{ lib
, callPackage, newScope
, writeText, runCommand, linkFarm, writeScript
, runtimeShell
, python3, jq
, crateUtils
, cargoManifestGenrationUtils
, icecapSrc, icecapExternalSrc
}:

let
  helpers = cargoManifestGenrationUtils;

  inherit (helpers) mkCrate;

  rawCratesList = import (icecapSrc.relativeRaw "rust/crates.nix") { inherit lib; };

  crates = lib.mapAttrs (name: relativePath:
    callCrate { inherit name relativePath; } (icecapSrc.relativeRaw "rust/${relativePath}/crate.nix") {}
  ) rawCratesList;

  localCratePathAttrs = lib.mapAttrs (_: crate: crate.path) crates;

  externalCratePathAttrs =
    let
      workspace = builtins.fromTOML (builtins.readFile (icecapSrc.relativeRaw "external/rust-seL4/Cargo.toml"));
    in
      lib.listToAttrs (lib.forEach workspace.workspace.members (relpath:
        let
          crate = builtins.fromTOML (builtins.readFile (icecapSrc.relativeRaw ("external/rust-seL4/" + relpath + "/Cargo.toml")));
        in
          {
            name = crate.package.name;
            value = "crates/external/rust-seL4/" + (lib.removePrefix "crates/" relpath);
          }
      ));

  cratePathAttrs = localCratePathAttrs // externalCratePathAttrs;

  cratePaths = lib.mapAttrs (name: path: { inherit name path; }) cratePathAttrs;

  callCrate =
    { name, relativePath
    }:

    let
      mk = args:
        assert args.package.name == name;
        mkCrate (crateUtils.clobber [
          {
            nix.path = relativePath;
            nix.frontmatter = crateFrontmatter;
            package = {
              edition = "2021";
              version = "0.1.0";
            };
          }
          args
        ]);

    in newScope rec {

      localCrates = cratePaths;

      inherit mk;

      versions = {
        anyhow = "1.0.66";
        cfg-if = "1.0.0";
        fallible-iterator = "0.2.0";
        heapless = "0.7.16";
        log = "0.4.17";
        num_enum = "0.5.9";
        object = "0.31.0";
        postcard = "1.0.2";
        proc-macro2 = "1.0.50";
        quote = "1.0.23";
        serde = "1.0.147";
        serde_json = "1.0.87";
        serde_yaml = "0.9.14";
        syn = "1.0.107";
        synstructure = "0.12.6";
        tock-registers = "0.8.1";
        zerocopy = "0.6.1";
      };

      serdeWith = features: {
        version = versions.serde;
        default-features = false;
        inherit features;
      };

      postcardWith = features: {
        version = versions.postcard;
        default-features = false;
        inherit features;
      };

      postcardCommon = postcardWith [ "alloc" ]; # HACK
    };

  crateFrontmatter = ''
    # This file is generated by the IceCap build system.
  '';

  workspaceTOML = helpers.renderManifest
    {
      frontmatter = workspaceFrontmatter;
      manifest = {
        workspace = {
          resolver = "2";
          members = lib.naturalSort (lib.attrValues cratePathAttrs);
        };
        profile.dev = {
          panic = "abort";
        };
      };
    };

  workspaceFrontmatter = ''
    # This file is generated by the IceCap build system.
  '';

  plan = {
    "Cargo.toml" = {
      src = workspaceTOML;
      justCheckEquivalenceWith = null;
    };
  } // lib.mapAttrs' (_: crate: {
    name = "${crate.path}/Cargo.toml";
    value = {
      src = crate.manifestTOML;
      justCheckEquivalenceWith = if crate.justEnsureEquivalence then helpers.checkTOMLEquivalence else null;
    };
  }) crates;

  script = actuallyDoIt: helpers.mkScript {
    inherit actuallyDoIt plan;
    root = toString (icecapSrc.relativeRaw "rust");
  };

  update = script true;
  check = script false;

in {
  inherit crates workspaceTOML externalCratePathAttrs;
  inherit update check;
}
