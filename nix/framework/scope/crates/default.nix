{ lib
, callPackage
, icecapSrc
, linkFarm, writeText
, crateUtils
, toAnonymousTOMLFile
}:

let
  workspaceDir = icecapSrc.relativeRaw "rust";

  workspaceManifest = builtins.fromTOML (builtins.readFile workspaceManifestPath);
  workspaceManifestPath = workspaceDir + "/Cargo.toml";
  workspaceMemberPaths = map (member: workspaceDir + "/${member}") workspaceManifest.workspace.members;

  overrides = {
    sel4-sys = {
      extraPaths = [
        "build"
      ];
    };
    sel4-bitfield-parser = {
      extraPaths = [
        "grammar.pest"
      ];
    };
    sel4-kernel-loader = {
      extraPaths = [
        "asm"
      ];
    };
    hypervisor-mirage = {
      extraPaths = [
        "c"
      ];
    };
  };

  crates = lib.listToAttrs (lib.forEach workspaceMemberPaths (cratePath: rec {
    name = (crateUtils.crateManifest cratePath).package.name; # TODO redundant
    value = crateUtils.mkCrate cratePath (overrides.${name} or {});
  }));

  augmentedCrates = crateUtils.augmentCrates crates;

in {
  inherit crates augmentedCrates;
}
