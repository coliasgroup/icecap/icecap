{ buildIceCapRustPackageIncrementally, globalCrates
}:

buildIceCapRustPackageIncrementally rec {
  rootCrate = globalCrates.crosvm-9p-server-cli;

  lastLayerModifications = {
    modifyDerivation = drv: drv.overrideAttrs (self: super: {
      passthru = (super.passthru or {}) // {
        exe = rootCrate.name;
      };
    });
  };
}
