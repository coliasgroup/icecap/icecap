{ lib, buildPackages, runCommand, writeText
, python3Packages

, icecapSrc, icecapExternalSrc

, icecapPlat
, seL4ForUserspace
, objectSizes

, sel4-simple-task-runtime-config-cli
, icecap-serialize-generic-component-config
}:

{ config
, script ? null
, command ? "python3 ${script}"
, computeUtCovers ? false
, extraNativeBuildInputs ? []
}:

let
  augmentedConfig = config // {
    kernel_config = "${seL4ForUserspace}/libsel4/include/kernel/gen_config.json";
    device_tree = "${seL4ForUserspace}/support/kernel.dtb";
    platform_info = "${seL4ForUserspace}/support/platform_gen.yaml";
    object_sizes = objectSizes;
    compute_ut_covers = computeUtCovers;
  };

  augmentedConfigJSON = writeText "config.json" (builtins.toJSON augmentedConfig);

  capdlSrc = icecapExternalSrc.capdl.extendInnerSuffix "python-capdl-tool";
  icedlSrc = icecapSrc.relative "python/local";
  capdlSimpleCompositionSrc = icecapSrc.relative "external/rust-seL4/hacking/src/python";

  # TODO
  pythonPathForShell = "";

in
lib.fix (self: runCommand "manifest" {

  nativeBuildInputs = [
    sel4-simple-task-runtime-config-cli
    icecap-serialize-generic-component-config
  ] ++ (with python3Packages; [
    future six
    aenum sortedcontainers
    pyyaml pyelftools pyfdt
  ]) ++ extraNativeBuildInputs;

  PYTHONPATH_ = lib.concatStringsSep ":" [ icedlSrc capdlSimpleCompositionSrc capdlSrc ];

  CONFIG = augmentedConfigJSON;

  passthru = {
    config = augmentedConfig;
    cdl = {
      spec = "${self}/spec.cdl";
      fill = "${self}/links";
    };
  };

  shellHook = ''
    export PYTHONPATH=${pythonPathForShell}:$PYTHONPATH
    export OUT_DIR=$(pwd)/x

    xxx() {
      ${command}
    }
  '';

} ''
  export PYTHONPATH=$PYTHONPATH_:$PYTHONPATH
  export OUT_DIR=$out
  echo $PYTHONPATH
  ${command}
'')
