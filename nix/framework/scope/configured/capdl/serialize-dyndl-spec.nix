{ runCommand
, capdl-tool
, icecap-serialize-dynamic-capdl-spec
, objectSizes
, icecapSrc
, serializeCapDLSpec
}:

{ spec, root, extraPassthru ? {} }:

let
  # exe = icecapSrc.localPathOf "tmp/capDL-tool/builddir/build/x86_64-linux/ghc-8.10.6/capDL-tool-1.0.0.1/x/parse-capDL/build/parse-capDL/parse-capDL";
  exe = "parse-capDL";

  json = serializeCapDLSpec {
    inherit spec;
  };

  bin = runCommand "dyndl_spec.bin" {
    nativeBuildInputs = [
      icecap-serialize-dynamic-capdl-spec
    ];
    passthru = {
      inherit json;
    } // extraPassthru;
  } ''
    icecap-serialize-dynamic-capdl-spec ${root} < ${json} > $out
  '';
in
  bin
