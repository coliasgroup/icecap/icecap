{ runCommand
, capdl-tool
, objectSizes
, icecapSrc
}:

{ spec }:

let
  # exe = icecapSrc.localPathOf "tmp/capDL-tool/builddir/build/x86_64-linux/ghc-8.10.6/capDL-tool-1.0.0.1/x/parse-capDL/build/parse-capDL/parse-capDL";
  exe = "parse-capDL";
in

runCommand "dyndl_spec.json" {
  nativeBuildInputs = [
    capdl-tool
  ];
} ''
  ${exe} --object-sizes=${objectSizes} --json=$out ${spec}
''
