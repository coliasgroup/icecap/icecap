{ runCommand
, capdl-tool
, objectSizes
, icecapSrc
, buildIceCapComponent, globalCrates
, serializeCapDLSpec
, crateUtils
, seL4ForUserspace
, seL4RustTargetInfoWithConfig
}:

let
  seL4Modifications = crateUtils.elaborateModifications {
    modifyDerivation = drv: drv.overrideAttrs (self: super: {
      SEL4_PREFIX = seL4ForUserspace;
    });
  };

in buildIceCapComponent {

  rootCrate = globalCrates.sel4-capdl-initializer;

  rustTargetInfo = seL4RustTargetInfoWithConfig { minimal = true; };

  # release = false;

  extraProfile = {
    panic = "abort";
    opt-level = 1; # bug on 2
  };

  # layers = [
  #   crateUtils.defaultIntermediateLayer
  #   {
  #     crates = [ "sel4-capdl-initializer-core" ];
  #     modifications = seL4Modifications;
  #   }
  # ];

}
