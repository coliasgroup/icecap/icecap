{ lib
, runCommand
, capdl-tool
, objectSizes
, icecapSrc
, serializeCapDLSpec
, crateUtils
, seL4ForUserspace
, sel4-capdl-initializer-add-spec
, capdl-loader
}:

{ spec, fill }:

let
  json = serializeCapDLSpec {
    inherit spec;
  };

in lib.fix (self: runCommand "armed-capdl-loader" {

  nativeBuildInputs = [
    sel4-capdl-initializer-add-spec
  ];

  passthru = {
    inherit spec json fill;
    split = {
      full = capdl-loader.split.full;
      min = self;
    };
  };

} ''
  sel4-capdl-initializer-add-spec -v -e ${capdl-loader.split.full} -f ${json} -d ${fill} --embed-frames -o $out
'')
