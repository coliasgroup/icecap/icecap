{ runCommand
, jq
, rustTargetPath, defaultRustTargetName
}:

runCommand "${defaultRustTargetName}.json" {
  nativeBuildInputs = [ jq ];
} ''
  f=${defaultRustTargetName}.json
  mkdir $out
  jq '."panic-strategy" = "abort" | ."requires-uwtable" = false | ."pre-link-args" = {}' < ${rustTargetPath}/$f > $out/$f
''
