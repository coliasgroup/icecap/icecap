{ lib, buildPackages
, buildIceCapRustPackageIncrementally, defaultRustTargetName, defaultRustTargetPath, crateUtils, elfUtils
, libclangPath
, seL4ForUserspace, globalCrates
, buildSysroot
, injectPhdrs
, defaultRustTargetInfo
}:

let
  injectPhdrs_ = injectPhdrs;
in

# NOTE
# To support unwinding with the 'unwinding' crate with non-GNU linkers:
# https://github.com/rust-lang/llvm-project/blob/b6b46f596a7d2523ee1acd1c00e699615849da60/libunwind/src/AddressSpace.hpp#L64

{ commonModifications ? {}
, lastLayerModifications ? {}

, rustTargetInfo ? defaultRustTargetInfo
, release ? true
, extraProfile ? {}
, replaceSysroot ? null
, injectPhdrs ? false
, ...
} @ args:

let
  rustTargetName = rustTargetInfo.name;
  rustTargetPath = rustTargetInfo.path;

  profile = if release then "release" else "dev";

  profiles = crateUtils.clobber [
    {
      profile.release = {
        lto = true;
      };
    }
    {
      profile.${profile} = {
        codegen-units = 1;
        # debug = 1; # TODO makes build take up too much space
        incremental = false;
      } // extraProfile;
    }
  ];

  sysroot = (if replaceSysroot != null then replaceSysroot else buildSysroot) {
    inherit rustTargetInfo release;
    extraManifest = profiles;
  };

  maybeInjectPhdrs = if injectPhdrs then injectPhdrs_ else lib.id;

  theseCommonModifications = crateUtils.elaborateModifications {
    modifyManifest = lib.flip lib.recursiveUpdate profiles;
    modifyConfig = lib.flip lib.recursiveUpdate {
      target.${rustTargetName}.rustflags = [
        "--sysroot" sysroot
      ];
    };
    modifyDerivation = drv: drv.overrideAttrs (self: super: {
      LIBCLANG_PATH = libclangPath;

      dontStrip = true;
      dontPatchELF = true;
    });
  };

  theseLastLayerModifications = crateUtils.elaborateModifications {
    modifyDerivation = drv: drv.overrideAttrs (self: super: {
      SEL4_PREFIX = seL4ForUserspace;

      passthru = (super.passthru or {}) // {
        elf = maybeInjectPhdrs "${self.finalPackage}/bin/${args.rootCrate.name}.elf";
        split = elfUtils.split self.finalPackage.elf;
      };
    });
  };

  prunedArgs = builtins.removeAttrs args [
    "extraProfile"
    "replaceSysroot"
    "injectPhdrs"
  ];

in

buildIceCapRustPackageIncrementally (prunedArgs // {

  commonModifications = crateUtils.composeModifications
    (crateUtils.elaborateModifications commonModifications)
    theseCommonModifications
  ;

  lastLayerModifications = crateUtils.composeModifications
    (crateUtils.elaborateModifications lastLayerModifications)
    theseLastLayerModifications
  ;
})
