{ lib, stdenv, buildPackages, buildPlatform, hostPlatform, runCommand, linkFarm, writeText
, rustToolchain
, toAnonymousTOMLFile, crateUtils, rustTargetPath, rustTargetPathMinimal, defaultRustTargetName

, sysrootVendoredLockfile
, icecapSrc
, fenix

, rustTargetName ? defaultRustTargetName
, release ? true
, useMinimalTarget ? false
, extraManifest ? {}
}:

let
  src = icecapSrc.repo {
    repo = "rust";
    rev = "e93ffadc7da7c12b75b1c1c1c949c3851a721c42"; # branch: icecap-sysroot
    sha256 = "sha256-Rz/FRu+FKhfTsKDLCcPAnLKkj0DhB9XITQ0/uuEAVbM=";
    submodules = true;
  };

  rustSrcPatched = linkFarm "rust-src-patched" [
    { name = "lib/rustlib/src/rust"; path = src; }
  ];

  rustToolchainPatched = fenix.combine [
    rustToolchain
    rustSrcPatched
  ];

  workspace = linkFarm "workspace" [
    { name = "Cargo.toml"; path = cargoToml; }
    { name = "Cargo.lock"; path = cargoLock; }
    { name = ".cargo/config"; path = cargoConfig; }
  ];

  package = {
    name = "dummy";
    version = "0.0.0";
  };

  cargoToml = toAnonymousTOMLFile (lib.recursiveUpdate {
    inherit package;

    lib.path = "${linkFarm "src" [
      (rec {
        name = "lib.rs";
        path = writeText name ''
          #![no_std]
        '';
      })
    ]}/lib.rs";

  } extraManifest);

  cargoLock = toAnonymousTOMLFile {
    package = [
      package
    ];
  };

  cargoConfig = toAnonymousTOMLFile (crateUtils.clobber [
    sysrootVendoredLockfile.configFragment
    {
      target = {
        ${rustTargetName} = {
          rustflags = [
            # "-C" "force-unwind-tables=yes" # TODO compare with "requires-uwtable" in target.json
            "-C" "embed-bitcode=yes"
            "--sysroot" "/dev/null"
          ];
        };
      };
    }
  ]);

in
runCommand "sysroot" {
  depsBuildBuild = [ buildPackages.stdenv.cc ];
  nativeBuildInputs = [ rustToolchainPatched ];

  RUST_TARGET_PATH = if useMinimalTarget then rustTargetPathMinimal else rustTargetPath;
} ''
  ln -s ${workspace}/.cargo .

  cargo build \
    --offline --frozen \
    -Z unstable-options \
    -Z build-std=core,alloc,std,compiler_builtins \
    -Z build-std-features=compiler-builtins-mem \
    --target ${rustTargetName} \
    ${lib.optionalString release "--release"} \
    --target-dir $(pwd)/target \
    --manifest-path ${workspace}/Cargo.toml

  d=$out/lib/rustlib/${rustTargetName}/lib
  mkdir -p $d
  mv target/${rustTargetName}/${if release then "release" else "debug"}/deps/* $d
''
