{ lib
, buildIceCapComponent, globalCrates
}:

let
  mk = crateName: attrs: buildIceCapComponent ({
    rootCrate = globalCrates.${crateName};
  } // attrs);

in
lib.mapAttrs mk {

  icecap-generic-timer-server = {};

}
