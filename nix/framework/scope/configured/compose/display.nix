{ lib, linkFarm
, composition
}:

with composition;

let
  sub = name': map ({ name, path }: {
    name = "${name'}/${name}";
    inherit path;
  });

  link = name: path: { inherit name path; };

  showSplit = name: { min, full }: [
    { name = "${name}.elf"; path = min; }
    { name = "${name}.debug.elf"; path = full; }
  ];

  showNotSplit = name: elf: [
    { name = "${name}.elf"; path = elf; }
  ];

in linkFarm "system" (lib.flatten [
  (link "image.elf" image)
  (sub "breakdown" (lib.flatten ([
    (link "seL4" attrs.seL4)
    (showSplit "loader" attrs.loaderELF)
    (showSplit "app" attrs.appELF)
  ] ++ lib.optionals (args ? config || args ? cdl) [
    (sub "components" (lib.flatten [
      (lib.mapAttrsToList showNotSplit cdlImages)
    ]))
    (sub "capdl-specification" (lib.flatten [
      (link "spec.cdl" "${attrs.cdl}/spec.cdl")
      # (link "icecap.spec.json" attrs.app.json) # TODO
      (link "frame-fill" "${attrs.cdl}/links")
      (link "workspace" attrs.cdl)
    ]))
  ])))
])
