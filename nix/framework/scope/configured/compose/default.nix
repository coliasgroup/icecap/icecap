{ lib, runCommand
, callPackage
, platUtils, elfUtils
, icecapPlat
, mkLoader
, mkIceDL, serializeCapDLSpec, mkCapDLLoader
, mkCapDLLoader'
, seL4ForBoot
, icecapExternalSrc, icecapFrameworkConfig
}:

args:

let
  attrs = lib.fix (self: {

    loader = mkLoader {
      inherit (self) seL4;
      appELF = self.appELF.min;
    };

    loaderELF = self.loader.split;

    seL4 = seL4ForBoot;

    appELF = self.app.split;

    app1 = mkCapDLLoader self.cdl.cdl;
    app2 = mkCapDLLoader' self.cdl.cdl;

    # useCapDLLoaderWithSerialization = false;
    useCapDLLoaderWithSerialization = true;

    app = if self.useCapDLLoaderWithSerialization then self.app2 else self.app1;

    script = null;
    config = null;

    cdl = mkIceDL {
      inherit (self) config script;
    };

    extra = _self: {};

  } // args);

  cdlImages = lib.mapAttrs'
    (k: v: lib.nameValuePair k v.image)
    (lib.filterAttrs (k: lib.hasAttr "image") attrs.cdl.config.components);

in lib.fix (self: with self; {
  inherit args attrs;
  inherit (attrs) cdl;

  image = attrs.loaderELF.min;

  inherit cdlImages;

  display = callPackage ./display.nix {
    composition = self;
  };
} // attrs.extra self)
