{ lib
, runCommand
, capdl-tool
, objectSizes
, serializeCapDLSpec
, crateUtils
, seL4ForBoot
, sel4-kernel-loader-add-payload
, loader-expecting-appended-payload
}:

{ seL4 ? seL4ForBoot, app }:

let

in lib.fix (self: runCommand "loader-with-serialization" {

  nativeBuildInputs = [
    sel4-kernel-loader-add-payload
  ];

  passthru = {
    elf = self;
    split = {
      full = loader-expecting-appended-payload.elf;
      min = self;
    };
  };

} ''
  sel4-kernel-loader-add-payload \
    -v \
    --loader ${loader-expecting-appended-payload.elf} \
    --sel4-prefix ${seL4ForBoot} \
    --app ${app} \
    -o $out
'')
