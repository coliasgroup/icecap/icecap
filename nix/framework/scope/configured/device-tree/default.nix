{ dtbHelpers, platUtils, selectIceCapPlat, devPkgs }:

let
  raspios = devPkgs.linuxHelpers.raspios; # HACK workaround for issue with nixpkgs gobject-introspection

in {
  seL4 = selectIceCapPlat {
    virt = dtbHelpers.decompile platUtils.virt.extra.dtb;
    rpi4 = dtbHelpers.decompile "${raspios.latest.boot}/bcm2711-rpi-4-b.dtb";
  };
}
