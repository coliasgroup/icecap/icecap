{ lib, makeOverridable'
, icecapSrc
, platUtils
, icecapExternalSrc, icecapFrameworkConfig
}:

icecapConfig:

self: with self;

{
  inherit icecapConfig;
  inherit (icecapConfig) icecapPlat;

  selectIceCapPlat = attrs: attrs.${icecapPlat};
  selectIceCapPlatOr = default: attrs: attrs.${icecapPlat} or default;

  compose = callPackage ./compose {};

  # TODO unify with parent scope deviceTree attribute
  deviceTreeConfigured = callPackage ./device-tree {};

  cmakeConfig = builtins.removeAttrs (callPackage ./sel4/cmake-config.nix {}) [ "override" "overrideDerivation" ];

  seL4Base = makeOverridable' (callPackage ./sel4 {}) {};
  seL4ForUserspace = seL4Base;
  # seL4ForUserspace = seL4ForBoot;
  seL4ForBoot = seL4Base.override' {
    # src = icecapExternalSrc.seL4.forceLocal;
    # out = icecapFrameworkConfig.source.localPathOf "seL4" + "/tmp/out";
  };

  libsel4 = "${seL4ForUserspace}/libsel4";

  mkLoader = { seL4 ? seL4ForBoot, appELF }: mkLoaderWithSerialization {
    app = appELF;
    inherit seL4;
  };

  mkLoaderWithSerialization = callPackage ./loader/mk-loader-with-serialization.nix {};

  loader-expecting-appended-payload = callPackage ./loader/loader-expecting-appended-payload.nix {
    loaderConfig = {};
  };

  serializeDynDLSpec = callPackage ./capdl/serialize-dyndl-spec.nix {};
  serializeCapDLSpec = callPackage ./capdl/serialize-capdl-spec.nix {};
  mkIceDL = callPackage ./capdl/mk-icedl.nix {};
  mkCapDLLoader = callPackage ./capdl/mk-capdl-loader.nix {};
  dummyCapDLSpec = callPackage ./capdl/dummy-spec.nix {};
  objectSizes = callPackage ./capdl/object-sizes.nix {};
  capdl-loader = callPackage ./capdl/capdl-loader.nix {};
  mkCapDLLoader' = callPackage ./capdl/mk-capdl-loader-with-serialization.nix {};

  dummyCapDLLoader = mkCapDLLoader dummyCapDLSpec.passthru;

  buildIceCapComponent = callPackage ./sel4-user/rust/build-icecap-component.nix {};
  genericComponents = callPackage ./sel4-user/rust/generic-components.nix {};

  # TODO generalize or leave up to downstream projects
  rustSysrootWithStd = callPackage ./sel4-user/rust/sysroot-with-std.nix {};

  rustTargetPathMinimal = callPackage ./sel4-user/rust/minimal-target.nix {};

  inherit (callPackage ./sel4-user/ocaml {}) mkMirageBinary;

}
