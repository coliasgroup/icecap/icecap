{ lib, stdenv, writeText, runCommandNoCC
, cmake, ninja
, dtc, libxml2
, python3, python3Packages

, icecapExternalSrc
, selectIceCapPlat
, deviceTreeConfigured
, cmakeConfig
}:

{ src ? icecapExternalSrc.seL4
, out ? null
}:

let
  settings = writeText "settings.cmake" ''
    ${lib.concatStrings (lib.mapAttrsToList (k: v: ''
      set(${k} ${v.value} CACHE ${v.type} "")
    '') cmakeConfig)}
  '';

  dtsVarName = selectIceCapPlat {
    rpi4 = "ICECAP_HACK_DTS";
    virt = "QEMU_DTS";
  };

  dts = deviceTreeConfigured.seL4;

  fake = lib.fix (self: runCommandNoCC "sel4-bundle-fake" {
    # passthru = mkPassthru self;
  } ''
    ln -s ${out} $out
  '');

  real = lib.fix (self: stdenv.mkDerivation {
    name = "seL4";

    inherit src;

    nativeBuildInputs = [
      cmake ninja
      dtc libxml2
      python3Packages.sel4-deps
    ];

    hardeningDisable = [ "all" ];

    postPatch = ''
      # patchShebangs can't handle env -S
      rm configs/*_verified.cmake

      patchShebangs --build .
    '';

    configurePhase = ''
      build=$(pwd)/build

      cmake \
        -DCROSS_COMPILER_PREFIX=${stdenv.cc.targetPrefix} \
        -DCMAKE_TOOLCHAIN_FILE=gcc.cmake \
        -DCMAKE_INSTALL_PREFIX=$out \
        -D${dtsVarName}=${dts} \
        -C ${settings} \
        -G Ninja \
        -B $build
    '';

    buildPhase = ''
      ninja -C $build all
    '';  

    installPhase = ''
      ninja -C $build install
    '';

    dontFixup = true;
  });

in if out != null then fake else real
