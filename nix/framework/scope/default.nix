{ lib, hostPlatform, callPackage, splicePackages, generateSplicesForMkScope
, qemu, fetchurl
, linuxHelpers
, icecapFramework
}:

let
  superCallPackage = callPackage;
in

self:

let
  callPackage = self.callPackage;
in

# Add nonePkgs, devPkgs, etc. to scope.
lib.mapAttrs' (k: lib.nameValuePair "${k}Pkgs") icecapFramework.pkgs //

# To avoid clutter, distribute scope accross multiple files.
superCallPackage ../../../src/external/rust-seL4/hacking/nix/rust-utils {} self //
superCallPackage ./rust {} self //

(with self; {

  cargoManifestGenrationUtils = callPackage ../../../src/external/rust-seL4/hacking/nix/cargo-manifest-generation-utils {};

  icecapFrameworkConfig = icecapFramework.config;

  icecapPlats = [
    "virt"
    "rpi4"
  ];

  byIceCapPlat = f: lib.listToAttrs (map (plat: lib.nameValuePair plat (f plat)) icecapPlats);

  elaborateIceCapConfig =
    { icecapPlat, icecapPlatParams ? {}
    , profile ? "icecap", debug ? false, benchmark ? false
    }: {
      inherit icecapPlat icecapPlatParams profile debug benchmark;
    };

  configure = icecapConfig: (lib.makeScope newScope (callPackage ./configured {} icecapConfig)).overrideScope' overrideConfiguredScope;

  overrideConfiguredScope = self: super: {};

  configured = byIceCapPlat (icecapPlat: makeOverridable' configure (elaborateIceCapConfig {
    inherit icecapPlat;
  }));

  inherit (callPackage ./source.nix {}) icecapSrc icecapExternalSrc;

  platUtils = byIceCapPlat (plat: callPackage (./plat-utils + "/${plat}") {});

  linuxOnly = assert hostPlatform.system == "aarch64-linux"; lib.id;

  uBoot = linuxOnly {
    host = byIceCapPlat (plat: callPackage (./u-boot + "/${plat}") {});
  };

  linuxKernel = linuxOnly {
    host = byIceCapPlat (plat: callPackage (./linux-kernel/host + "/${plat}") {});
    guest.minimal = callPackage ./linux-kernel/guest/minimal {};
  };

  nixosLite = callPackage ./linux-user/nixos-lite {};

  crosvm-9p-server = callPackage ./linux-user/crosvm-9p-server.nix {};

  capdl-tool = callPackage ./dev/capdl-tool {};
  sel4-manual = callPackage ./dev/sel4-manual.nix {};

  mkTool = rootCrate: buildIceCapRustPackageIncrementally {
    inherit rootCrate;
    release = false; # speed up build
  };

  sel4-inject-phdrs = mkTool globalCrates.sel4-inject-phdrs;
  icecap-serialize-dynamic-capdl-spec = mkTool globalCrates.icecap-serialize-dynamic-capdl-spec;
  sel4-backtrace-cli = mkTool globalCrates.sel4-backtrace-cli;
  sel4-simple-task-runtime-config-cli = mkTool globalCrates.sel4-simple-task-runtime-config-cli;
  icecap-serialize-generic-component-config = mkTool globalCrates.icecap-serialize-generic-component-config;
  sel4-capdl-initializer-add-spec = mkTool globalCrates.sel4-capdl-initializer-add-spec;
  sel4-kernel-loader-add-payload = mkTool globalCrates.sel4-kernel-loader-add-payload;

  inherit (callPackage ./stdenv {}) mkStdenv stdenvMusl stdenvBoot stdenvToken stdenvMirage;

  musl = callPackage ./stdenv/musl.nix {};

  crates = callPackage ./crates {};
  globalCrates = crates.augmentedCrates;

  generatedCargoManifests = callPackage ./generated-cargo-manifests {};

  nixUtils = callPackage ./nix-utils {};
  elfUtils = callPackage ./nix-utils/elf-utils.nix {};
  cmakeUtils = callPackage ./nix-utils/cmake-utils.nix {};
  injectPhdrs = callPackage ./nix-utils/inject-phdrs.nix {};

  inherit (nixUtils) callWith makeOverridable';

  # Re-export for convenience
  inherit (linuxHelpers) dtbHelpers raspios raspberry-pi-firmare;

  # Relegate ocaml to sub-scope
  ocamlScope =
    let
      superOtherSplices = otherSplices;
    in
    let
      otherSplices = with superOtherSplices; {
        selfBuildBuild = selfBuildBuild.ocamlScope;
        selfBuildHost = selfBuildHost.ocamlScope;
        selfBuildTarget = selfBuildTarget.ocamlScope;
        selfHostHost = selfHostHost.ocamlScope;
        selfTargetTarget = selfTargetTarget.ocamlScope or {};
      };
    in
      lib.makeScopeWithSplicing
        splicePackages
        newScope
        otherSplices
        (_: {})
        (_: {})
        (self: callPackage ./ocaml {} self // {
          __dontMashWhenSplicingChildren = true;
          __dontSpliceChildren = true; # for performance
          inherit superOtherSplices otherSplices; # for convenience
        })
      ;

  inherit (ocamlScope) icecap-ocaml-runtime;

  nixpkgsOld = import (builtins.fetchGit {
    url = "https://github.com/NixOS/nixpkgs.git";
    rev = "2f388220dc53c7c8a8d15b1c865ebd16bfe87356";
    ref = "master";
  }) {};

  thisQemu = nixpkgsOld.qemu;
  # thisQemu = qemu;

  qemuForIceCap = (thisQemu.override (attrs: {
    hostCpuTargets = [ "aarch64-softmmu" ];
    guestAgentSupport = false;
    numaSupport = false;
    seccompSupport = false;
    alsaSupport = false;
    pulseSupport = false;
    sdlSupport = false;
    jackSupport = false;
    gtkSupport = false;
    vncSupport = false;
    smartcardSupport = false;
    spiceSupport = false;
    ncursesSupport = false;
    usbredirSupport = false;
    xenSupport = false;
    cephSupport = false;
    glusterfsSupport = false;
    openGLSupport = false;
    virglSupport = false;
    libiscsiSupport = false;
    smbdSupport = false;
    tpmSupport = false;
    uringSupport = false;
  })).overrideDerivation (attrs: {
    patches = attrs.patches ++ [
      # Augment qemu -M virt with a simple timer device model and a simple channel device model
      (fetchurl {
        url = "https://gitlab.com/coliasgroup/icecap/qemu/-/commit/7f706948179f16267396a65541cfc580f2b30e70.patch";
        sha256 = "sha256-mWnXvCT1ZWp3js+EVQj4MjzoVZ9znEIUZgSHkctt3K4==";
      })
    ];
  });

})
