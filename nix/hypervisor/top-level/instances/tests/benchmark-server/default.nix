{ mkInstance
, icecapSrc
, icecapExternalSrc
, globalCrates
}:

mkInstance { benchmark = true; } (self: with self.configured; with self; {

  composition = compose {
    # inherit (self) kernel;
    cdl = mkHypervisorIceDL {
      script = icecapSrc.absolute ./cdl.py;
      config = {
        components = {
          test.image = test.elf;
          benchmark_server.image = hypervisorComponents.benchmark-server.elf;
        };
      };
    };
  };

  test = buildIceCapComponent {
    rootCrate = globalCrates.hypervisor-tests-capdl-benchmark-server-test;
    release = false;
  };

  # NOTE example of how to develop on the seL4 kernel source
  # kernel = configured.kernel.override' {
  #   source = icecapExternalSrc.seL4.forceLocal;
  # };

})
