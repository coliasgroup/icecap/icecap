{ mkInstance
, commonModules
, linuxPkgs
, icecapSrc, icecapExternalSrc
}:

mkInstance {} (self: with self;

let
  oldSrc = builtins.fetchGit {
    url = toString icecapSrc.root;
    rev = "712f6fc6ca99e06f19f0ce4e9f92ad8aed1f0ab9";
    submodules = true;
  };

  old = import oldSrc;

  test = old.hypervisor.tests.hypervisor.${configured.icecapPlat};

in {

  inherit (test) hostPayload;

  composition = configured.icecapFirmware.override' {
    inherit (test.composition.attrs) cdl;
  };

})
