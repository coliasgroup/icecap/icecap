{ buildIceCapRustPackageIncrementally
, callPackage
}:

buildIceCapRustPackageIncrementally rec {
  rootCrate = callPackage ./crate.nix {};
  release = false;
}
