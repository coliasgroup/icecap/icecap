{ lib, buildIceCapRustPackageIncrementally, globalCrates
}:

buildIceCapRustPackageIncrementally rec {
  rootCrate = globalCrates.icecap-host;

  commonModifications = {
    modifyManifest = lib.flip lib.recursiveUpdate {
      profile.release = {
        codegen-units = 1;
        lto = true;
      };
    };
  };
}
