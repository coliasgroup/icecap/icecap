{ lib, hostPlatform
, icecapSrc
, vendorLockfileFromFile
, libfdt
, defaultRustTargetName
, rustToolchain
, mkShell
, stdenv, buildPackages
, cargo, rustc, git, cacert, python3
, crateUtils, toAnonymousTOMLFile
}:

let
  rustTargetName = hostPlatform.config;

  src = icecapSrc.repo {
    repo = "firecracker";
    rev = "2532d50d9afb8ff2e9084f3345d263ab457c88fb";
  };

  cargoConfig = toAnonymousTOMLFile (crateUtils.clobber [
    (crateUtils.linkerConfig { inherit rustToolchain rustTargetName; })
    (vendorLockfile { lockfile = "${src}/Cargo.lock"; }).configFragment
    {
      # TODO why is this necessary?
      target.${hostPlatform.config}.fdt = {
        rustc-link-search = [ "native=${libfdt}/lib" ];
      };
    }
  ]);

in
stdenv.mkDerivation {
  pname = "firecracker";
  version = "0.25.0";
  inherit src;

  nativeBuildInputs = [ rustToolchain ];
  buildInputs = [ libfdt ];

  dontConfigure = true;
  dontInstall = true;
  dontFixup = true;

  buildPhase = ''
    mkdir -p .cargo && ln -s ${cargoConfig} .cargo/config

    cargo build \
      -Z unstable-options \
      --offline --frozen \
      --release \
      --target ${target} \
      --out-dir $out/bin \
      -j $NIX_BUILD_CORES
  '';

  postPatch = ''
    rm -r .cargo
  '';
}
