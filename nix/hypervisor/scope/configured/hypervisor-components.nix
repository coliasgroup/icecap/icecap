{ lib
, buildIceCapComponent, globalCrates
, genericComponents
}:

let
  mk = name: attrs: buildIceCapComponent ({
    rootCrate = globalCrates."hypervisor-${name}";
  } // attrs);

in
lib.mapAttrs mk {

  fault-handler = {};
  serial-server = {};
  timer-server = {};
  event-server = {};
  resource-server = {};
  benchmark-server = {};
  host-vmm = {};
  realm-vmm = {};
  idle = {};

} // {

  timer-server = genericComponents.icecap-generic-timer-server;

}
