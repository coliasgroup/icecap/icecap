{ mkHypervisorIceDL, serializeDynDLSpec
, icecapPlat, icecapSrc
}:

{ script, config }:

let
  ddl = mkHypervisorIceDL {
    script = icecapSrc.absolute script;
    inherit config;
  };

in
serializeDynDLSpec {
  spec = "${ddl}/spec.cdl";
  root = "${ddl}/links";
  extraPassthru = {
    inherit ddl;
  };
}
