PLAT ?= virt

out := out

ifeq ($(K),1)
	keep_going := -k
endif

ifneq ($(J),)
	jobs := -j$(J)
endif

nix_build = nix-build $(keep_going) $(jobs)

.PHONY: all
all: hypervisor-firmware

$(out):
	mkdir -p $@

.PHONY: clean
clean:
	rm -rf $(out)

.PHONY: hypervisor-firmware
hypervisor-firmware: | $(out)
	$(nix_build) -A hypervisor.framework.pkgs.none.icecap.configured.$(PLAT).icecapFirmware.display -o $(out)/$@-$(PLAT)

.PHONY: show-hypervisor-tcb-size
show-hypervisor-tcb-size:
	report=$$($(nix_build) -A hypervisor.tcbSize --no-out-link) && cat $$report

.PHONY: everything
everything: check-generated-sources
	$(nix_build) -A everything.all --no-out-link

.PHONY: everything-pure
everything-pure:
	$(nix_build) -A everything.pure --no-out-link

.PHONY: everything-cached
everything-cached:
	$(nix_build) -A everything.cached --no-out-link

.PHONY: everything-with-excess
everything-with-excess: check-generated-sources
	$(nix_build) -A everything.allWithExcess --no-out-link

.PHONY: check-generated-sources
check-generated-sources:
	script=$$($(nix_build) -A framework.generatedSources.check --no-out-link) && $$script

.PHONY: update-generated-sources
update-generated-sources:
	script=$$($(nix_build) -A framework.generatedSources.update --no-out-link) && $$script
	cd src/rust && cargo update -w

.PHONY: run-automated-tests
run-automated-tests:
	script=$$($(nix_build) -A framework.automatedTests.runAll --no-out-link) && $$script
	script=$$($(nix_build) -A hypervisor.automatedTests.runAll --no-out-link) && $$script
