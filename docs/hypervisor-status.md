# Hypervisor status

The current implementation of the IceCap Hypervisor is a prototype. This
implementation serves as a research vehicle for virtualization-based
confidential computing. It is not yet suitable for production use.
